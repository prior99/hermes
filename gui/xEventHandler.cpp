/*
 * xEventHandler.cpp
 *
 *  Created on: Sep 24, 2013
 *      Author: sascha
 */

#include"xEventHandler.hpp"

EventHandler::EventHandler()
{

}
EventHandler::~EventHandler()
{

}

void EventHandler::handleEvents(void)
{
	SDL_Event event;

	while (SDL_PollEvent(&event))
	{
		switch (event.type)
		{
		case SDL_QUIT:
			onExit();
			break;
		case SDL_KEYDOWN:
			onKeyTyped(event.key.keysym.sym);
			//KEYS[event.key.keysym.sym] = 1;
			break;
		case SDL_KEYUP:
			//donothing
			//KEYS[event.key.keysym.sym] = 0;
			break;
		//case SDL_VIDEORESIZE:
		//    SCREEN_X = event.resize.w;
		//    SCREEN_Y = event.resize.h;
		//    screen_updateSize();
		case SDL_DROPFILE:      //  In case of dropped file
			onFileDropped(event.drop.file);
			break;
		case SDL_MOUSEBUTTONDOWN:
			//onMouseClicked(event.button.x, event.button.y);
			onMouseDownAt(event.button.x, event.button.y);
			break;
		case SDL_MOUSEBUTTONUP:
			onMouseUpAt(event.button.x, event.button.y);
			break;
		case SDL_MOUSEMOTION:
			onMouseMotion(event.motion.x, event.motion.y, event.motion.xrel, event.motion.yrel);
		case SDL_WINDOWEVENT:
			if(event.window.event == SDL_WINDOWEVENT_RESIZED)
			{
				onWindowResize(event.window.data1, event.window.data2);
			}
			break;
		default:
			break;
		}
	}
}

void EventHandler::onMouseDownAt(int x, int y) //popupboxes have vorrang
{
	if(msgs.empty())
	{
		for(std::deque<Box*>::iterator i=distMB.begin(); i!=distMB.end(); i++)
		{
			(*i)->mouseDownAt(x,y);
		}
	}
	else
	{
		if(msgs.back()->mouseDownAt(x-msgs.back()->getPosX(), y-msgs.back()->getPosY()) != 0)
		{
			delete msgs.back();
			msgs.pop_back();
		}
	}
}
void EventHandler::onMouseUpAt(int x, int y)
{
	for(std::deque<Box*>::iterator i=distMB.begin(); i!=distMB.end(); i++)
	{
		(*i)->mouseUpAt(x,y);
	}
}
void EventHandler::onMouseMotion(int x, int y, int rx, int ry)
{
	for(std::deque<Box*>::iterator i=distMM.begin(); i!=distMM.end(); i++)
	{
		(*i)->mouseMotion(x,y, rx, ry);
	}
}
void EventHandler::onKeyTyped(SDL_Keycode k) //popupboxes!
{
	if(msgs.empty())
	{
		for(std::deque<Box*>::iterator i=distKT.begin(); i!=distKT.end(); i++)
		{
			(*i)->keyTyped(k);
		}
	}
	else
	{
		if(msgs.back()->keyTyped(k) != 0)
		{
			delete msgs.back();
			msgs.pop_back();
		}
	}
}
std::deque<Box*> EventHandler::getMsgs(void)
{
    return msgs;
}

#include<iostream>
void signinSomethingTo(Box* nc, std::deque<Box*>& box)
{
	if(nc)
	{
		for(std::deque<Box*>::iterator i=box.begin(); i!=box.end(); i++)
		{
		    std::cout << "Box already added" << std::endl;
			if(*i == nc) return;
		}
		//std::cout << "Box empty:" << box.empty() << std::endl;
		box.push_back(nc);
	    //std::cout << "Box empty:" << box.empty() << std::endl;
		//std::cout << "Box added" << std::endl;
	}
}
void signoutSomethingFrom(Box* nc, std::deque<Box*>& box)
{
	if(nc)
	{
		int c = 0;
		for(std::deque<Box*>::iterator i=box.begin(); i!=box.end(); i++, c++)
		{
			if(*i == nc)
			{
				box.erase(box.begin()+c);
				return;
			}
		}
	}
}

//adding distibuters to boxes

void EventHandler::signinMouseButtonListener(Box* nc)
{
	signinSomethingTo(nc, distMB);
}
void EventHandler::signoutMouseButtonListener(Box* nc)
{
	signoutSomethingFrom(nc, distMB);
}
void EventHandler::signinMouseMotionListener(Box* nc)
{
	signinSomethingTo(nc, distMM);
}
void EventHandler::signoutMouseMotionListener(Box* nc)
{
	signoutSomethingFrom(nc, distMM);
}
void EventHandler::signintKeycodeListener(Box* nc)
{
	signinSomethingTo(nc, distKT);
}
void EventHandler::signoutKeycodeListener(Box* nc)
{
	signoutSomethingFrom(nc, distKT);
}
void EventHandler::signinPopupBox(Box* nc)
{
	signinSomethingTo(nc, msgs);
	//std::cout << "msgs is empty: " << msgs.empty() << std::endl;
}
void EventHandler::signoutPopupBox(Box* nc)
{
	signoutSomethingFrom(nc, msgs);
}




