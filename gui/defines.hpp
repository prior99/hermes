/*
 * defines.hpp
 *
 *  Created on: Sep 23, 2013
 *      Author: sascha
 */

#ifndef DEFINES_HPP_
#define DEFINES_HPP_

#define LISTCOLOR
#define ROWCOLOR
#define BARCOLOR
#define MAINCOLOR
#define PROGCOLOR
#define SCROLLLISTCOLOR
#define OKCOLOR
#define INPUTCOLOR
#define ACCEPTCOLOR
#define IDCOLOR
#define LABELCOLOR

#define PADDING 1

#endif /* DEFINES_HPP_ */
