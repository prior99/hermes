/*
 * xEventHaldler.hpp
 *
 *  Created on: Sep 24, 2013
 *      Author: sascha
 */

#ifndef XEVENTHALDLER_HPP_
#define XEVENTHALDLER_HPP_

#include"xSDL.hpp"
#include"msgBoxes/box.hpp"
#include"defines.hpp"
#include"msgBoxes/component.hpp"
#include<queue>

class EventHandler
{
private:
	std::deque<Box*> distMB;
	std::deque<Box*> distMM;
	std::deque<Box*> distKT;
	
	std::deque<Box*> msgs; //total priority to MD KT
public:
	EventHandler();
	virtual ~EventHandler();
	void handleEvents(void);
    std::deque<Box*>getMsgs(void);

	virtual void onMouseDownAt(int, int);
	virtual void onMouseUpAt(int, int);
	virtual void onMouseMotion(int, int, int, int);
	virtual void onKeyTyped(SDL_Keycode);
	virtual void onFileDropped(const char*) = 0;
	virtual void onWindowResize(int, int) = 0;
	virtual void onExit() = 0;

	void signinMouseButtonListener(Box*);
	void signoutMouseButtonListener(Box*);
	void signinMouseMotionListener(Box*);
	void signoutMouseMotionListener(Box*);
	void signintKeycodeListener(Box*);
	void signoutKeycodeListener(Box*);
	void signinPopupBox(Box*);
	void signoutPopupBox(Box*);
	//possible components that need to be supplied with events

};




#endif /* XEVENTHALDLER_HPP_ */
