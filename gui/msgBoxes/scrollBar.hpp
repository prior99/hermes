/*
 * ScrollBaronent.hpp
 *
 *  Created on: Sep 20, 2013
 *      Author: sascha
 */

#ifndef ScrollBarONENT_HPP_
#define ScrollBarONENT_HPP_

#include"component.hpp"
#include<string>

#define SIZE_BAR 30
#define XSIZESCROLL 25

class ScrollBar : public Component
{
private:
	int position;
	bolo mouseDown;
public:
	ScrollBar();
	~ScrollBar();
	bolo update(int, int);
	int mouseDownAt(int, int);
	int mouseMotion(int, int, int, int);
	int mouseUpAt(int, int);
	int getPosition(void); //returns 0..100
};

#endif /* ScrollBarONENT_HPP_ */
