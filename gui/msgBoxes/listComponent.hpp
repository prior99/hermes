/*
 * listComponent.hpp
 *
 *  Created on: Sep 19, 2013
 *      Author: sascha
 */

#ifndef LISTCOMPONENT_HPP_
#define LISTCOMPONENT_HPP_

#include<vector>
#include"component.hpp"

class ListComp : public Component
{
private:
	std::vector<Component*> components;
public:
	ListComp();
	~ListComp();
	void addComponent(Component*);
	void removeComponent(Component*);
	bolo update(int, int);

	static const int NO_PREFERENCE;
	static const int POS_CENTERED;
	static const int POS_BOUNDLEFT; //implicit default
	static const int POS_BOUNDRIGHT;
};


#endif /* LISTCOMPONENT_HPP_ */
