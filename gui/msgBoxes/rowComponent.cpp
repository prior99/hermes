/*
 * component.cpp
 *
 *  Created on: Sep 19, 2013
 *      Author: sascha
 */

#include"rowComponent.hpp"

const int RowComp::NO_PREFERENCE = 0;
const int RowComp::POS_CENTERED = 1;
const int RowComp::POS_BOUNDTOP = 2;
const int RowComp::POS_BOUNDBOTTOM = 4;

RowComp::RowComp()
{
	setSurface(NULL);
}
RowComp::~RowComp()
{

}
void RowComp::addComponent(Component* xm)
{
	components.push_back(xm);
}
void RowComp::removeComponent(Component* xm)
{
	//removecomment by pointer...
}
bolo RowComp::update(int sx, int sy)
{
	int tx = min(sx, getPrefX());
	int ty = min(sy, getPrefY());
	//setSize(tx, ty); //tx ty backup

	int compn = components.size();
	tx -= 2*PADDING; //rest of prefsize

	int ax = 0; //actual
	int ay = 0;
	for(std::vector<Component*>::iterator vcp=components.begin(); vcp!=components.end(); vcp++)
	{
		(*vcp)->update(tx/compn, ty-2*PADDING); //assuming compn != 0 else SOFT ERROR!
		compn--;
		tx -= (*vcp)->getX();
		ax += (*vcp)->getX();
		ay = max(ay, (*vcp)->getY());
	}
	ax+=2*PADDING;
	ay+=2*PADDING;

	setSize(ax, ay);
	checkSurface(ax, ay);
	fillColorWithBorder(getSurface(), 20, 20, 20);
	tx = PADDING;

	for(std::vector<Component*>::iterator vcp=components.begin(); vcp!=components.end(); vcp++)
	{
		switch((*vcp)->getPreference())
		{
		case RowComp::POS_BOUNDBOTTOM:
			(*vcp)->setPos(getPosX()+tx, getPosY()+ay-(PADDING+(*vcp)->getY()));
			renderTextureOnto((*vcp)->getSurface(), getSurface(), tx, ay - (PADDING+(*vcp)->getY()), (*vcp)->getX(), (*vcp)->getY());
			break;
		case RowComp::POS_CENTERED:
			(*vcp)->setPos(getPosX()+tx, getPosY()+(ay - (*vcp)->getY())/2);
			renderTextureOnto((*vcp)->getSurface(), getSurface(), tx, (ay - (*vcp)->getY())/2, (*vcp)->getX(), (*vcp)->getY());
			break;
		case RowComp::NO_PREFERENCE:
		case RowComp::POS_BOUNDTOP:
		default:
			(*vcp)->setPos(getPosX()+tx, getPosY()+PADDING);
			renderTextureOnto((*vcp)->getSurface(), getSurface(), tx, PADDING, (*vcp)->getX(), (*vcp)->getY());
			break;
		}
		tx += (*vcp)->getX();
	}
	return ax<=sx && ay<=sy;
}
