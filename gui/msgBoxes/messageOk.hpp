/*
 * messageOk.hpp
 *
 *  Created on: Sep 19, 2013
 *      Author: sascha
 */

#ifndef MSGOKACCEPT_HPP_
#define MSGOKACCEPT_HPP_

#include"box.hpp"

class MsgOk : public Box
{
private:
	Vec2 posOk;
	Vec2 sizeBox;
public:
	MsgOk(char*);
	~MsgOk();
	int mouseDownAt(int, int);
	int keyTyped(SDL_Keycode);
};

#endif
