#include"labelComponent.hpp"

LabelComp::LabelComp()
{
	setSurface(NULL);
	text = " ";
}
LabelComp::LabelComp(std::string nstr)
{
	setSurface(NULL);
	text = nstr;
}
LabelComp::~LabelComp()
{
	if(getSurface() != NULL) SDL_FreeSurface(getSurface());
}
bolo LabelComp::update(int sx, int sy)
{
	SDL_Surface* tmpTxt = createTextSurface(text, 0);
	int tx = max(min(sx, getPrefX()), tmpTxt->w+2*PADDING);
	int ty = max(min(sy, getPrefY()), tmpTxt->h+2*PADDING);

	setSize(tx, ty);
	checkSurface(tx, ty);
	fillColorWithBorder(getSurface(), 30, 30, 30);

	renderTextureOnto(tmpTxt, getSurface(), (getX()-tmpTxt->w) / 2, (getY()-tmpTxt->h) / 2, tmpTxt->w, tmpTxt->h);
	SDL_FreeSurface(tmpTxt);
	return tx<=sx && ty <=sy;
}
void LabelComp::updateText(std::string ntext)
{
	text = ntext;
}
