#ifndef __MSGBOXINCL__
#define __MSGBOXINCL__

#include<cstdlib>
#include"../xSDL.hpp"
#include"../defines.hpp"

typedef bool bolo;
//typedef void (*StrFunc) (std::string);

typedef struct Vec2 Vec2;
struct Vec2 {
	int x;
	int y;
};

class Box
{
private:
	Vec2 size;
	Vec2 pos;
	SDL_Surface* msg;
public:
	virtual ~Box();

	void setSize(int x, int y);
	int getX(void);
	int getY(void);

	void setPos(int x, int y);
	int getPosX(void);
	int getPosY(void);

	virtual int mouseDownAt(int, int);
	virtual int mouseMotion(int, int, int, int);
	virtual int mouseUpAt(int, int);
	virtual int keyTyped(SDL_Keycode);

	SDL_Surface* getSurface(void);
	void setSurface(SDL_Surface* nmsg);

};

#endif
