/*
 * MsgAccept.hpp
 *
 *  Created on: Sep 19, 2013
 *      Author: sascha
 */

#ifndef MSGINPUT_HPP_
#define MSGINPUTT_HPP_

#include"box.hpp"
#include "../guiListener.hpp"

class MsgInput : public Box
{
private:
	std::string input;
	std::string text;
	Vec2 posAcc;
	Vec2 posDec;
	Vec2 sizeBox;
	GUIListener* listener;
	void redraw();

public:
	MsgInput(std::string, GUIListener*);
	~MsgInput();
	int mouseDownAt(int, int);
	int keyTyped(SDL_Keycode);
};

#endif
