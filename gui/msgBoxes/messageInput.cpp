/*
 * messageInput.cpp
 *
 *  Created on: Sep 19, 2013
 *      Author: sascha
 */

#include "messageInput.hpp"
#include "../guiScreen.hpp"
#include <iostream>
#include <sstream>
#define PREFX 300

using namespace std;

MsgInput::MsgInput(std::string ntext, GUIListener *listener)
{
	this->listener = listener;
	//f = nf;
	input = "";
	text = ntext;
	redraw();
}
void MsgInput::redraw()
{
	stringstream s("");
	s << text << ": " << input;
	SDL_Surface* tmptex = createTextSurface(s.str().c_str(), 0);
	SDL_Surface* tmpacc = createTextSurface("Ok", 0);
	SDL_Surface* tmpdec = createTextSurface("Cancel", 0);

	int wtex = max(tmptex->w + 4*PADDING, PREFX);
	int htex = tmptex->h + 4*PADDING;

	sizeBox.x = max(tmpacc->w,tmpdec->w)+4*PADDING;
	sizeBox.y = max(tmpacc->h,tmpdec->h)+4*PADDING;

	//textrectangle
	SDL_Surface* tmpmsg = createSDLSurface(wtex, htex);
	fillColor(tmpmsg, 50,50,50); //black
	renderTextureOnto(tmptex, tmpmsg, 2*PADDING, 2*PADDING, wtex-4*PADDING, htex-4*PADDING);
	SDL_FreeSurface(tmptex);
	//Input button
	SDL_Surface* tmpbtna = createSDLSurface(sizeBox.x, sizeBox.y);
	fillColor(tmpbtna, 124, 252, 0); //green
	renderTextureOnto(tmpacc, tmpbtna, 2*PADDING, 2*PADDING, sizeBox.x-4*PADDING, sizeBox.y-4*PADDING);
	SDL_FreeSurface(tmpacc);
	//decline button
	SDL_Surface* tmpbtnd = createSDLSurface(sizeBox.x, sizeBox.y);
	fillColor(tmpbtnd, 252, 124, 0); //red
	renderTextureOnto(tmpdec, tmpbtnd, 2*PADDING, 2*PADDING, sizeBox.x-4*PADDING, sizeBox.y-4*PADDING);
	SDL_FreeSurface(tmpdec);

	int w = max(wtex, 2*sizeBox.x+PADDING) + 2*PADDING;
	int h = htex + sizeBox.y + 3*PADDING;

	posAcc.y = posDec.y = h - (PADDING + sizeBox.y);
	posAcc.x = (PADDING + w) / 2 - sizeBox.x; posDec.x = (PADDING + w) / 2;

	setSize(w, h);

	setSurface(createSDLSurface(w, h));
	fillColorWithBorder(getSurface(), 40, 40, 40); //black
	renderTextureOnto(tmpmsg, getSurface(), (w-wtex) / 2, PADDING, wtex, htex);
	renderTextureOnto(tmpbtna, getSurface(), posAcc.x, posAcc.y, sizeBox.x, sizeBox.y);
	renderTextureOnto(tmpbtnd, getSurface(), posDec.x, posDec.y, sizeBox.x, sizeBox.y);

	SDL_FreeSurface(tmpmsg);
	SDL_FreeSurface(tmpbtna);
	SDL_FreeSurface(tmpbtnd);
}
MsgInput::~MsgInput()
{
	SDL_FreeSurface(getSurface());
}

int MsgInput::keyTyped(SDL_Keycode k)
{
	switch(k)
	{
	case SDLK_BACKSPACE:
		if(input.size()>0) input = std::string(input.c_str(), input.size()-1);
		redraw();
		break;
	case SDLK_ESCAPE:
		input = "";
		listener->idInputConfirmed("");
		return -1;
		break;
	case SDLK_RETURN:
		listener->idInputConfirmed(input);
		return 1;
		break;
	default:
		input += keyCodeToString(k);
		while(input.size()>4) input = std::string(input.c_str(), input.size()-1);
		redraw();
		//std::cout << input << std::endl;
		break;
	}
	return 0;
}


int MsgInput::mouseDownAt(int x, int y)
{
	if(x>posAcc.x && x<posAcc.x+sizeBox.x)
	{
		if(y>posAcc.y && y<posAcc.y+sizeBox.y)
		{
			listener->idInputConfirmed(input);
			return 1;
		}
	}
	else if(x>posDec.x && x<posDec.x+sizeBox.x)
	{
		if(y>posDec.y && y<posDec.y+sizeBox.y)
		{
			listener->idInputConfirmed("");
			return -1;
		}
	}
	return 0;
}
