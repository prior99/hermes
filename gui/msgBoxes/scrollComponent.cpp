/*
 * component.cpp
 *
 *  Created on: Sep 19, 2013
 *      Author: sascha
 */

#include"scrollComponent.hpp"
#include <iostream>

const int ScrollComp::NO_PREFERENCE = 0;
const int ScrollComp::POS_CENTERED = 1;
const int ScrollComp::POS_BOUNDLEFT = 2;
const int ScrollComp::POS_BOUNDRIGHT = 4;

#define HARD_MINIMALSIZE 150

ScrollComp::ScrollComp()
{
	setSurface(NULL);
}
ScrollComp::~ScrollComp()
{

}
void ScrollComp::addComponent(Component* xm)
{
	components.push_back(xm);
}
void ScrollComp::removeComponent(Component* xm)
{
	//removecomment by pointer...
}
bolo ScrollComp::update(int sx, int sy)
{
	//hardcoded minimal size 150?
	int tx = min(sx, getPrefX());
	int ty = min(sy, getPrefY());

	int compn = components.size();
	int dy = ty - 2*PADDING; //rest of prefsize

	int ax = 0; //actual
	int ay = 0;
	for(std::vector<Component*>::iterator vcp=components.begin(); vcp!=components.end(); vcp++)
	{
		(*vcp)->update(tx-PADDING-XSIZESCROLL, dy / compn); //assuming compn != 0 else SOFT ERROR!
		compn--;
		dy -= (*vcp)->getY();
		ay += (*vcp)->getY();
		ax = max(ax, (*vcp)->getX());
	}

	SDL_Surface* tsurf = createSDLSurface(ax, ay);
	fillColorWithBorder(tsurf, 255, 0, 255);
	ay = 0;

	for(std::vector<Component*>::iterator vcp=components.begin(); vcp!=components.end(); vcp++)
	{
		switch((*vcp)->getPreference())
		{
		case ScrollComp::POS_BOUNDRIGHT:
			renderTextureOnto((*vcp)->getSurface(), tsurf, ax - (*vcp)->getX(), ay, (*vcp)->getX(), (*vcp)->getY());
			(*vcp)->setPos(ax-(*vcp)->getX(), ay);
			break;
		case ScrollComp::POS_CENTERED:
			renderTextureOnto((*vcp)->getSurface(), tsurf, (ax - (*vcp)->getX()) / 2, ay, (*vcp)->getX(), (*vcp)->getY());
			(*vcp)->setPos((ax-(*vcp)->getX())/2, ay);
			break;
		case ScrollComp::NO_PREFERENCE:
		case ScrollComp::POS_BOUNDLEFT:
		default:
			renderTextureOnto((*vcp)->getSurface(), tsurf, 0, ay, (*vcp)->getX(), (*vcp)->getY());
			(*vcp)->setPos(0, ay);
			break;
		}
		ay += (*vcp)->getY();
	}
	//axay actual
	//txty current selected

	if(ay<ty)
	{
		setSize(ax+2*PADDING, ay+2*PADDING);
		checkSurface(getX(), getY());
		fillColorWithBorder(getSurface(), 20, 20, 20);
		renderTextureOnto(tsurf, getSurface(), PADDING, PADDING, ax, ay);
		SDL_FreeSurface(tsurf);
		for(std::vector<Component*>::iterator vcp=components.begin(); vcp!=components.end(); vcp++)
	    {
	        (*vcp)->setPos((*vcp)->getPosX()+PADDING+getPosX(), (*vcp)->getPosY()+PADDING+getPosY());
	    }
	}
	else
	{
		scroll.update(0, max(150, ty));
		ty = scroll.getY();
		tx = ax + XSIZESCROLL + PADDING;
		setSize(tx, ty);
		checkSurface(tx, ty);
		fillColorWithBorder(getSurface(), 20, 20, 20);

		int tpos = ay-ty+2*PADDING;
		int tposstepoffset = (tpos*(scroll.getPosition()))/100;

		renderTextureWithOffset(tsurf, getSurface(), PADDING, PADDING, ax, ay, 0, tposstepoffset);
		SDL_FreeSurface(tsurf);
		for(std::vector<Component*>::iterator vcp=components.begin(); vcp!=components.end(); vcp++)
	    {
	        (*vcp)->setPos((*vcp)->getPosX()+PADDING+getPosX()-tposstepoffset, (*vcp)->getPosY()+PADDING+getPosY());
	    }
		scroll.setPos(getPosX()+PADDING+ax, getPosY()+0);
		renderTextureOnto(scroll.getSurface(), getSurface(), PADDING+ax, 0, scroll.getX(), scroll.getY());
	}
	return getX()<=sx && getY()<=sy;
}
int ScrollComp::mouseDownAt(int x, int y)
{
	//mouseDown = x>PADDING && x<getX()-PADDING && y>PADDING
	//need exact bar position... + to every component!
	
	//std::cout << "mouse down scrollcomp at: " << x << " " << y << std::endl;
	//std::cout << "absolute scomp pos: " << getPosX() << " " << getPosY() << std::endl;
	if(x>getPosX()&&x<getPosX()+getX()&&y>getPosY()&&y<getPosY()+getY())
	{
	    scroll.mouseDownAt(x, y);
	    int c = 0;
	    for(std::vector<Component*>::iterator vcp=components.begin(); vcp!=components.end(); vcp++, c++)
    	{
    	    //std::cout << "checking component: " << (*vcp)->getPosX() << " " << (*vcp)->getPosY() << std::endl;
    	    if(x>(*vcp)->getPosX()&&x<(*vcp)->getPosX()+(*vcp)->getX()&&y>(*vcp)->getPosY()&&y<(*vcp)->getPosY()+(*vcp)->getY())
    	    {
    	        //std::cout << "found component" << std::endl;
    	        if((*vcp)->mouseDownAt(x, y))
    	        {
    	            std::cout << "Deleting finished bar ... ";
    	            delete (*vcp);
    	            components.erase(components.begin()+c);
    	            std::cout << "done." << std::endl;
    	        }
    	        return 0; //so iterator does not fuck up
    	    }
	    }
	}
	//std::cout << "all components checked" << std::endl;
	return 0;
}
int ScrollComp::mouseMotion(int, int, int, int ry)
{
	scroll.mouseMotion(0, 0, 0, ry); //rx, x,y not needed , vertical bar!!
	return 0;
}
int ScrollComp::mouseUpAt(int, int)
{
	scroll.mouseUpAt(0, 0); // x, y, not needed! //ONLY HERE!
	return 0;
}
