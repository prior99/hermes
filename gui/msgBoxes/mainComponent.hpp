/*
 * mainComponent.hpp
 *
 *  Created on: Sep 20, 2013
 *      Author: sascha
 */

#ifndef MAINCOMPONENT_HPP_
#define MAINCOMPONENT_HPP_

#include"listComponent.hpp"
#include"labelComponent.hpp"
#include"rowComponent.hpp"
#include"idComponent.hpp"
#include"scrollComponent.hpp"

class MainComp : public Component
{
private:
	IdComp* idbar; //idcomp
	LabelComp* status; //label
	ScrollComp* progbars; //list
public:
	MainComp(IdComp*, LabelComp*, ScrollComp*);
	~MainComp();
	bolo update(int, int);

	/*static const int NO_PREFERENCE;
	static const int POS_CENTERED;
	static const int POS_BOUNDLEFT; //implicit default
	static const int POS_BOUNDRIGHT;*/
};

#endif /* MAINCOMPONENT_HPP_ */
