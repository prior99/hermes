/*
 * component.cpp
 *
 *  Created on: Sep 19, 2013
 *      Author: sascha
 */

#include"listComponent.hpp"

const int ListComp::NO_PREFERENCE = 0;
const int ListComp::POS_CENTERED = 1;
const int ListComp::POS_BOUNDLEFT = 2;
const int ListComp::POS_BOUNDRIGHT = 4;

ListComp::ListComp()
{
	setSurface(NULL);
}
ListComp::~ListComp()
{

}
void ListComp::addComponent(Component* xm)
{
	components.push_back(xm);
}
void ListComp::removeComponent(Component* xm)
{
	//removecomment by pointer...
}
bolo ListComp::update(int sx, int sy)
{
	int tx = min(sx, getPrefX());
	int ty = min(sy, getPrefY());
	//setSize(tx, ty); //tx ty backup

	int compn = components.size();
	int dy = ty - 2*PADDING; //rest of prefsize

	int ax = 0; //actual
	int ay = 0;
	for(std::vector<Component*>::iterator vcp=components.begin(); vcp!=components.end(); vcp++)
	{
		(*vcp)->update(tx-2*PADDING, dy / compn); //assuming compn != 0 else SOFT ERROR!
		compn--;
		dy -= (*vcp)->getY();
		ay += (*vcp)->getY();
		ax = max(ax, (*vcp)->getX());
	}
	ax = max(ax+2*PADDING, tx);
	ay = max(ay+2*PADDING, ty);

	setSize(ax, ay);
	checkSurface(ax, ay);
	fillColorWithBorder(getSurface(), 20, 20, 20);
	ty = PADDING;

	for(std::vector<Component*>::iterator vcp=components.begin(); vcp!=components.end(); vcp++)
	{
		switch((*vcp)->getPreference())
		{
		case ListComp::POS_BOUNDRIGHT:
			(*vcp)->setPos(getPosX()+ax - (PADDING+(*vcp)->getX()), getPosY()+ty);
			renderTextureOnto((*vcp)->getSurface(), getSurface(), ax - (PADDING+(*vcp)->getX()), ty, (*vcp)->getX(), (*vcp)->getY());
			break;
		case ListComp::POS_CENTERED:
			(*vcp)->setPos(getPosX()+(ax - (*vcp)->getX()) / 2, getPosY()+ty);
			renderTextureOnto((*vcp)->getSurface(), getSurface(), (ax - (*vcp)->getX()) / 2, ty, (*vcp)->getX(), (*vcp)->getY());
			break;
		case ListComp::NO_PREFERENCE:
		case ListComp::POS_BOUNDLEFT:
		default:
			(*vcp)->setPos(getPosX()+PADDING, getPosY()+ty);
			renderTextureOnto((*vcp)->getSurface(), getSurface(), PADDING, ty, (*vcp)->getX(), (*vcp)->getY());
			break;
		}
		ty += (*vcp)->getY();
	}
	return ax<=sx && ay<=sy;
}
//#include<iostream>
