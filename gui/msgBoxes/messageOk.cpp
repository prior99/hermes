/*
 * messageOk.cpp
 *
 *  Created on: Sep 19, 2013
 *      Author: sascha
 */

#include"messageOk.hpp"
#include"../guiScreen.hpp"

#define PREFX 300

MsgOk::MsgOk(char* text)
{
	SDL_Surface* tmptex = createTextSurface(text, 0);
	SDL_Surface* tmpok = createTextSurface("Ok", 0);

	int wtex = max(PREFX, tmptex->w + 4*PADDING);
	int htex = tmptex->h + 4*PADDING;

	sizeBox.x = tmpok->w+4*PADDING;
	sizeBox.y = tmpok->h+4*PADDING;

	//textrectangle
	SDL_Surface* tmpmsg = createSDLSurface(wtex, htex);
	fillColor(tmpmsg, 50,50,50); //black
	renderTextureOnto(tmptex, tmpmsg, 2*PADDING, 2*PADDING, wtex-4*PADDING, htex-4*PADDING);
	SDL_FreeSurface(tmptex);
	//ok button
	SDL_Surface* tmpbtn = createSDLSurface(sizeBox.x, sizeBox.y);
	fillColor(tmpbtn, 124, 252, 0); //green
	renderTextureOnto(tmpok, tmpbtn, 2*PADDING, 2*PADDING, sizeBox.x-4*PADDING, sizeBox.y-4*PADDING);
	SDL_FreeSurface(tmpok);

	int w = max(wtex, sizeBox.x) + 2*PADDING;
	int h = htex + sizeBox.y + 3*PADDING;

	setSize(w, h);

	posOk.y = h - (PADDING + sizeBox.y);
	posOk.x = (w - sizeBox.x) / 2;

	setSurface(createSDLSurface(w, h));
	fillColorWithBorder(getSurface(), 25, 25, 25); //black
	renderTextureOnto(tmpmsg, getSurface(), (w - wtex) / 2, PADDING, wtex, htex);
	renderTextureOnto(tmpbtn, getSurface(), posOk.x, posOk.y, sizeBox.x, sizeBox.y);

	SDL_FreeSurface(tmpmsg);
	SDL_FreeSurface(tmpbtn);
}
MsgOk::~MsgOk()
{
	SDL_FreeSurface(getSurface());
}
#include<iostream>
int MsgOk::mouseDownAt(int x, int y)
{
	if(x>posOk.x && x<posOk.x+sizeBox.x)
	{
		if(y>posOk.y && y<posOk.y+sizeBox.y)
		{
			std::cout << "Clicked ok!!" << std::endl;
			return 1;
		}
	}
	return 0;
}
int MsgOk::keyTyped(SDL_Keycode k)
{
	switch(k)
	{
	case SDLK_ESCAPE:
		return -1;
		break;
	default:
		break;
	}
	return 0;
}
