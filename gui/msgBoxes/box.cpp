/*
 * messageBox.cpp
 *
 *  Created on: Sep 19, 2013
 *      Author: sascha
 */

#include"box.hpp"

void Box::setSize(int x, int y)
{
	size.x = x;
	size.y = y;
}

int Box::getX(void)
{
	return size.x;
}
int Box::getY(void)
{
	return size.y;
}

void Box::setPos(int x, int y)
{
	pos.x = x;
	pos.y = y;
}

int Box::getPosX(void)
{
	return pos.x;
}
int Box::getPosY(void)
{
	return pos.y;
}
//#include<iostream>
int Box::mouseDownAt(int, int)
{
    //std::cout << "baseclass mousedown called" << std::endl;
    return 0;
}
int Box::mouseMotion(int, int, int, int)
{
    return 0;
}
int Box::mouseUpAt(int, int)
{
    return 0;
}
int Box::keyTyped(SDL_Keycode)
{
    return 0; 
}
SDL_Surface* Box::getSurface(void)
{
    return msg;
}
void Box::setSurface(SDL_Surface* nmsg)
{
    msg=nmsg;
}

Box::~Box()
{

}
