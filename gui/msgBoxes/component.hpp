/*
 * component.hpp
 *
 *  Created on: Sep 19, 2013
 *      Author: sascha
 */

#ifndef COMPONENT_HPP_
#define COMPONENT_HPP_

#include"box.hpp"

class Component : public Box
{
private:
	int preference;
	int prefx;
	int prefy;
public:
	Component() : preference(0), prefx(5000), prefy(5000) {} //very big pref, so the initial setting is "fill"
	virtual ~Component() {}
	virtual bool update(int, int) = 0;
	void setPreference(int p)
	{
		preference = p;
	}
	void setPreferredSize(int px, int py)
	{
		prefx=px;
		prefy=py;
	}
	int getPrefX(void)
	{
		return prefx;
	}
	int getPrefY(void)
	{
		return prefy;
	}
	int getPreference(void)
	{
		return preference;
	}
	void checkSurface(int sx, int sy)
	{
		if(getSurface()==NULL)
		{
			setSurface(createSDLSurface(sx, sy));
		}
		else if(getSurface()->w != sx || getSurface()->h != sy)
		{
			SDL_FreeSurface(getSurface());
			setSurface(createSDLSurface(sx, sy));
		}
	}
};


#endif /* COMPONENT_HPP_ */
