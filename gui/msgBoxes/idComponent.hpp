/*
 * IdComponent.hpp
 *
 *  Created on: Sep 20, 2013
 *      Author: sascha
 */

#ifndef IDCOMPONENT_HPP_
#define IDCOMPONENT_HPP_

#include"component.hpp"
#include<string>

class IdComp : public Component
{
private:
	std::string id;
public:
	IdComp();
	IdComp(std::string);
	~IdComp();
	bolo update(int, int);
	void updateId(std::string);
};

#endif /* IdCOMPONENT_HPP_ */
