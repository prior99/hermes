/*
 * MsgAccept.hpp
 *
 *  Created on: Sep 19, 2013
 *      Author: sascha
 */

#ifndef MSGACCEPT_HPP_
#define MSGACCEPT_HPP_

#include"box.hpp"

class MsgAccept : public Box
{
private:
	int choice;

	Vec2 posAcc;
	Vec2 posDec;
	Vec2 sizeBox;
public:
	MsgAccept(char*);
	~MsgAccept();
	int mouseDownAt(int, int);
	bolo getChoice(); //DO !!NOT!! call from GUI-thread!!!
	int keyTyped(SDL_Keycode);
};

#endif /* MSGACCEPT_HPP_ */
