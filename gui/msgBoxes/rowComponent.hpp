/*
 * listComponent.hpp
 *
 *  Created on: Sep 19, 2013
 *      Author: sascha
 */

#ifndef ROWCOMPONENT_HPP_
#define ROWCOMPONENT_HPP_

#include<vector>
#include"component.hpp"

class RowComp : public Component
{
private:
	std::vector<Component*> components;
public:
	RowComp();
	~RowComp();
	void addComponent(Component*);
	void removeComponent(Component*);
	bolo update(int, int);

	static const int NO_PREFERENCE;
	static const int POS_CENTERED;
	static const int POS_BOUNDTOP; //implicit default
	static const int POS_BOUNDBOTTOM;
};


#endif
