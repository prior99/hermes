/*
 * ScrollComponent.hpp
 *
 *  Created on: Sep 19, 2013
 *      Author: sascha
 */

#ifndef ScrollCOMPONENT_HPP_
#define ScrollCOMPONENT_HPP_

#include<vector>
#include"component.hpp"
#include"scrollBar.hpp"

class ScrollComp : public Component //DOES NOT SUPPORT POSITION-EVENTPASSING
{
private:
	std::vector<Component*> components;
	ScrollBar scroll;
public:
	ScrollComp();
	~ScrollComp();
	void addComponent(Component*);
	void removeComponent(Component*);
	bolo update(int, int);

	int mouseDownAt(int, int);
	int mouseUpAt(int, int);
	int mouseMotion(int, int, int, int);

	static const int NO_PREFERENCE;
	static const int POS_CENTERED;
	static const int POS_BOUNDLEFT; //implicit default
	static const int POS_BOUNDRIGHT;
};


#endif /* ScrollCOMPONENT_HPP_ */
