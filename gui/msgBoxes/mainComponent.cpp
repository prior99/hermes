/*
* mainComponent.cpp
*
*  Created on: Sep 20, 2013
*      Author: sascha
*/

#include"mainComponent.hpp"

/*const int MainComp::NO_PREFERENCE = 0;
const int MainComp::POS_CENTERED = 1;
const int MainComp::POS_BOUNDLEFT = 2;
const int MainComp::POS_BOUNDRIGHT = 4;*/

MainComp::MainComp(IdComp* a, LabelComp* c, ScrollComp* b)
{
	setSurface(NULL);
	idbar = a;
	progbars = b;
	status = c;
}
MainComp::~MainComp()
{
	SDL_FreeSurface(getSurface());
}

/*********************************+
 *
 *  +---------------------------+
 *  | +-your-id-++-status-msgs+ |
 *  | +-----------------------+ |
 *  | |                       | |
 *  | |       current         | |
 *  | |  progressbars         | |
 *  | |                       | |
 *  | +-----------------------+ |
 *  | +---status-messages-----+ |
 *  +---------------------------+
 *
 *
 *
 */
//#include<iostream>
bolo MainComp::update(int sx, int sy)
{
	int tx = sx - 2*PADDING;
	int ty = sy - 2*PADDING;

	idbar->update(tx, ty);
	//std::cout << "idbar " << idbar->getX() << " " << idbar->getY() << std::endl;
	status->update(tx - idbar->getX(), ty);
	//std::cout << "status " << status->getX() << " " << status->getY() << std::endl;

	ty -= max(status->getY(), idbar->getY());

	progbars->update(tx, ty);
	//std::cout << "progbars " << progbars->getX() << " " << progbars->getY() << std::endl;

	tx = max(sx, 2*PADDING + max(idbar->getX()+status->getX(), progbars->getX()));
	ty = max(max(idbar->getY(), status->getY()) + progbars->getY() + 2*PADDING, sy);
	//std::cout << tx << " " << ty << std::endl;

	checkSurface(tx, ty);
	setSize(tx, ty);
	fillColorWithBorder(getSurface(), 10, 10, 10);
	
	renderTextureOnto(idbar->getSurface(), getSurface(), PADDING, PADDING, idbar->getX(), idbar->getY());
	idbar->setPos(PADDING, PADDING);
	renderTextureOnto(status->getSurface(), getSurface(), PADDING+idbar->getX(), PADDING, status->getX(), status->getY());
    status->setPos(PADDING+idbar->getX(), PADDING);
	renderTextureOnto(progbars->getSurface(), getSurface(), PADDING, PADDING+max(idbar->getY(), status->getY()), progbars->getX(), progbars->getY());
	progbars->setPos(PADDING, PADDING+max(idbar->getY(), status->getY()));

	/*switch(idbar->getPreference())
	{
	case POS_BOUNDRIGHT:
		idbar->setPos(tx - PADDING - idbar->getX(), PADDING);
		renderTextureOnto(idbar->getSurface(), getSurface(), tx - PADDING - idbar->getX(), PADDING, idbar->getX(), idbar->getY());
		break;
	case POS_CENTERED:
		idbar->setPos((tx - idbar->getX()) / 2, PADDING);
		renderTextureOnto(idbar->getSurface(), getSurface(), (tx - idbar->getX()) / 2, PADDING, idbar->getX(), idbar->getY());
		break;
	case NO_PREFERENCE:
	case POS_BOUNDLEFT:
	default:
		idbar->setPos(PADDING, PADDING);
		renderTextureOnto(idbar->getSurface(), getSurface(), PADDING, PADDING, idbar->getX(), idbar->getY());
		break;
	}

	progbars->setPos(PADDING, PADDING+idbar->getY());
	renderTextureOnto(progbars->getSurface(), getSurface(), PADDING, PADDING+idbar->getY(), progbars->getX(), progbars->getY());

	switch(status->getPreference())
	{
	case POS_BOUNDRIGHT:
		status->setPos(tx-PADDING-status->getX(), getY() - (PADDING+status->getY()));
		renderTextureOnto(status->getSurface(), getSurface(), tx - PADDING - status->getX(), getY() - (PADDING+status->getY()), status->getX(), status->getY());
		break;
	case POS_CENTERED:
		status->setPos((tx - status->getX()) / 2, getY() - (PADDING+status->getY()));
		renderTextureOnto(status->getSurface(), getSurface(), (tx - status->getX()) / 2, getY() - (PADDING+status->getY()), status->getX(), status->getY());
		break;
	case NO_PREFERENCE:
	case POS_BOUNDLEFT:
	default:
		status->setPos(PADDING, getY() - (PADDING+status->getY()));
		renderTextureOnto(status->getSurface(), getSurface(), PADDING, getY() - (PADDING+status->getY()), status->getX(), status->getY());
		break;
	}*/
	return true;
}
