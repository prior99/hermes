/*
 * progressBar.hpp
 *
 *  Created on: Sep 19, 2013
 *      Author: sascha
 */

#ifndef PROGRESSBAR_HPP_
#define PROGRESSBAR_HPP_

#include"../xSDL.hpp"
#include"component.hpp"
#include<string>
#include<cmath>

class ProgressBar : public Component
{
private:
	int progress; //in percent 1..100
	std::string file;
	bolo direction;
	bolo readyToRemove;
	//bolo parts[100];
public:
	ProgressBar(std::string, bolo);
	virtual ~ProgressBar();
	void updateProgress(int);
	//void updateParts(bolo*);
	//derived
	bolo update(int, int);
	
	void remove(void); //allows bar to be removed 
	
	int mouseDownAt(int, int);

	static const bool PDOWNLOAD;
	static const bool PUPLOAD;
};


#endif /* PROGRESSBAR_HPP_ */
