/*
 * labelComponent.hpp
 *
 *  Created on: Sep 20, 2013
 *      Author: sascha
 */

#ifndef LABELCOMPONENT_HPP_
#define LABELCOMPONENT_HPP_

#include"component.hpp"
#include<string>

class LabelComp : public Component
{
private:
	std::string text;
public:
	LabelComp();
	LabelComp(std::string);
	~LabelComp();
	bolo update(int, int);
	void updateText(std::string);
};

#endif /* LABELCOMPONENT_HPP_ */
