#include"idComponent.hpp"

IdComp::IdComp()
{
	setSurface(NULL);
	id = " ";
}
IdComp::IdComp(std::string nstr)
{
	setSurface(NULL);
	id = nstr;
}
IdComp::~IdComp()
{
	if(getSurface() != NULL) SDL_FreeSurface(getSurface());
}
bolo IdComp::update(int sx, int sy)
{
	SDL_Surface* tmpTxt = createTextSurface(id.c_str(), 1);
	int tx = max(min(sx, getPrefX()), tmpTxt->w+2*PADDING);
	int ty = max(min(sy, getPrefY()), tmpTxt->h+2*PADDING);

	setSize(tx, ty);
	checkSurface(tx, ty);
	fillColorWithBorder(getSurface(), 30, 30, 30);

	renderTextureOnto(tmpTxt, getSurface(), (getX()-tmpTxt->w) / 2, (getY()-tmpTxt->h) / 2, tmpTxt->w, tmpTxt->h);
	SDL_FreeSurface(tmpTxt);
	return tx<=sx && ty<=sy;
}
void IdComp::updateId(std::string ntext)
{
	id = ntext;
}
