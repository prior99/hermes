/*
 * messageAccept.cpp
 *
 *  Created on: Sep 19, 2013
 *      Author: sascha
 */

#include"messageAccept.hpp"
#include <iostream>
#include"../guiScreen.hpp"

#define PREFX 400

MsgAccept::MsgAccept(char* text)
{
	choice = 0;
	SDL_Surface* tmptex = createTextSurface(text, 0);
	SDL_Surface* tmpacc = createTextSurface("Accept File", 0);
	SDL_Surface* tmpdec = createTextSurface("Decline File", 0);

	int wtex = max(tmptex->w + 4*PADDING, PREFX);
	int htex = tmptex->h + 4*PADDING;

	sizeBox.x = max(tmpacc->w,tmpdec->w)+4*PADDING;
	sizeBox.y = max(tmpacc->h,tmpdec->h)+4*PADDING;

	//textrectangle
	SDL_Surface* tmpmsg = createSDLSurface(wtex, htex);
	fillColor(tmpmsg, 50,50,50); //black
	renderTextureOnto(tmptex, tmpmsg, 2*PADDING, 2*PADDING, wtex-4*PADDING, htex-4*PADDING);
	SDL_FreeSurface(tmptex);
	//accept button
	SDL_Surface* tmpbtna = createSDLSurface(sizeBox.x, sizeBox.y);
	fillColor(tmpbtna, 124, 252, 0); //green
	renderTextureOnto(tmpacc, tmpbtna, 2*PADDING, 2*PADDING, sizeBox.x-4*PADDING, sizeBox.y-4*PADDING);
	SDL_FreeSurface(tmpacc);
	//decline button
	SDL_Surface* tmpbtnd = createSDLSurface(sizeBox.x, sizeBox.y);
	fillColor(tmpbtnd, 252, 124, 0); //red
	renderTextureOnto(tmpdec, tmpbtnd, 2*PADDING, 2*PADDING, sizeBox.x-4*PADDING, sizeBox.y-4*PADDING);
	SDL_FreeSurface(tmpdec);

	int w = max(wtex, 2*sizeBox.x+PADDING) + 2*PADDING;
	int h = htex + sizeBox.y + 3*PADDING;

	posAcc.y = posDec.y = h - (PADDING + sizeBox.y);
	posAcc.x = (PADDING + w) / 2 - sizeBox.x; posDec.x = (PADDING + w) / 2;

	setSize(w, h);

	setSurface(createSDLSurface(w, h));
	fillColorWithBorder(getSurface(), 25, 25, 25); //black
	renderTextureOnto(tmpmsg, getSurface(), (w-wtex) / 2, PADDING, wtex, htex);
	renderTextureOnto(tmpbtna, getSurface(), posAcc.x, posAcc.y, sizeBox.x, sizeBox.y);
	renderTextureOnto(tmpbtnd, getSurface(), posDec.x, posDec.y, sizeBox.x, sizeBox.y);

	SDL_FreeSurface(tmpmsg);
	SDL_FreeSurface(tmpbtna);
	SDL_FreeSurface(tmpbtnd);
}
MsgAccept::~MsgAccept()
{
	SDL_FreeSurface(getSurface());
}
bolo MsgAccept::getChoice() //DO !!NOT!! call from GUI-thread!!!
{
	while(choice == 0) SDL_Delay(100);
	return choice == 1;
}
#include<iostream>
int MsgAccept::mouseDownAt(int x, int y)
{
	if(x>posAcc.x && x<posAcc.x+sizeBox.x)
	{
		if(y>posAcc.y && y<posAcc.y+sizeBox.y)
		{
			choice = 1;
			std::cout << "Clicked accept!!" << std::endl;
			return 0; //do not delete box on confirmed input
		}
	}
	else if(x>posDec.x && x<posDec.x+sizeBox.x)
	{
		if(y>posDec.y && y<posDec.y+sizeBox.y)
		{
			choice = -1;
			std::cout << "Clicked decline!!" << std::endl;
			return 0;
		}

	}
	return 0;
}
int MsgAccept::keyTyped(SDL_Keycode k)
{
	switch(k)
	{
	case SDLK_ESCAPE:
		choice = -1;
		return 0;
		break;
	case SDLK_RETURN:
		choice = 1;
		return 0;
		break;
	}
	return 0;
}
