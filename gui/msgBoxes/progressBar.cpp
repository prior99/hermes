#include"progressBar.hpp"
#include<sstream>
#include<iostream>

const bool ProgressBar::PUPLOAD = false;
const bool ProgressBar::PDOWNLOAD = true;

ProgressBar::ProgressBar(std::string nfile, bolo b)
{
	file = nfile;
	std::cout << file << " is being transferred." << std::endl;
	setSurface(NULL);
	progress = 0;
	direction = b;
	setSize(0,0);
	readyToRemove = false;
	setPreferredSize(5000,50);
}

ProgressBar::~ProgressBar()
{
	SDL_FreeSurface(getSurface());
}
/*void ProgressBar::updateParts(bolo* b)
{
	for(int i=0; i!=100; i++)
	{
		parts[i] = b[i];
	}
}*/
void ProgressBar::remove(void)
{
    readyToRemove = true;
}
void ProgressBar::updateProgress(int p)
{
	progress = p;
}
bolo ProgressBar::update(int sx, int sy)
{

	std::ostringstream oss;
	if(direction) oss << "Download";
	else oss << "Upload";
	oss << " '" << file << "': " << progress << "%";
	SDL_Surface* percent = createTextSurface(oss.str().c_str(), 0);

	int tx = max(min(sx, getPrefX()), percent->w+6*PADDING);
	int ty = max(min(sy, getPrefY()), 2*PADDING+2*(percent->h+4*PADDING));
	setSize(tx, ty);
	checkSurface(tx, ty);

	fillColorWithBorder(getSurface(), 40, 40, 40);


	renderTextureOnto(percent, getSurface(), 3*PADDING, 3*PADDING, percent->w, percent->h);

	int tmpy = ty-6*PADDING-percent->h;
	SDL_Rect offset;
	offset.h = tmpy-2*PADDING;
	offset.w = floor((float)progress*((float)(tx-4*PADDING)/100.0f));
	if(direction) offset.x = PADDING;
	else offset.x = getX() - (offset.w + PADDING);
	offset.y = PADDING;

	SDL_Surface* bar1 = createSDLSurface(tx-2*PADDING, tmpy);
	fillColorWithBorder(bar1, 20,20,20);
	fillColorWithOffset(bar1, &offset, 255, progress, 0);
	renderTextureOnto(bar1, getSurface(), PADDING, 5*PADDING+percent->h, bar1->w, bar1->h);

	/*SDL_Surface* bar2 = createSDLSurface(tx-2*PADDING, tmpy);
	fillColorWithBorder(bar2, 20,20,20); //PARTS_BAR
	offset.w = max(bar2->w/100, 1);
	for(offset.x=PADDING; offset.x<bar2->w-PADDING; offset.x+=offset.w)
	{
		if(parts[((offset.x-PADDING)*100)/bar2->w])
		{
			fillColorWithOffset(bar2, &offset, 255, progress, 0);
		}
	}
	renderTextureOnto(bar2, getSurface(), PADDING, 5*PADDING+percent->h+tmpy, bar2->w, bar2->h);*/

	SDL_FreeSurface(bar1);
    //SDL_FreeSurface(bar2);
	SDL_FreeSurface(percent);
	return tx<=sx && ty<=sy;
}

int ProgressBar::mouseDownAt(int x, int y)
{
    if(readyToRemove) return 1;
    else std::cout << "Filetransfer '" << file << "' still active! Cannot close progress bar" << std::endl;
    return 0;
}
