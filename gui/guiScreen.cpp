/*
 * guiScreen.cpp
 *
 *  Created on: Sep 18, 2013
 *      Author: sascha
 */
#include"guiScreen.hpp"
#include"msgBoxes/progressBar.hpp"
#include"msgBoxes/messageInput.hpp"
#include<iostream>
#include<math.h>
#include<string>
#include<sstream>
#include "../client/client.hpp"


GUIScreen::GUIScreen(void) : mainc(&idbar, &statusBar, &list)
{
    std::cout << "Init SDL...";

    dead = false;

    SCREEN_X = 500;
    SCREEN_Y = 400;

    if( SDL_Init( SDL_INIT_EVERYTHING ) < 0 )
    {
        std::cout << "Video initialization failed: " << SDL_GetError() << std::endl;
        exit(-1);
    }
    //SDL_WINDOWPOS_CENTERED,SDL_WINDOWPOS_CENTERED
    SDL_CreateWindowAndRenderer(SCREEN_X, SCREEN_Y,SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE, &window, &renderer);
    if(!window || !renderer)
    {
    	std::cout << "Something has gone wrong: " << SDL_GetError() << std::endl;
    	exit(-1);
    }
    SDL_ShowCursor(SDL_ENABLE);
    SDL_SetWindowTitle(window, "Hermes");

    texture = SDL_CreateTexture(renderer, SDL_PIXELFORMAT_ARGB8888, SDL_TEXTUREACCESS_STATIC, SCREEN_X, SCREEN_Y);

    loadPixelFormat();

    if(!texture)
    {
    	std::cout << "Texture fail: " << SDL_GetError();
    	exit(-1);
    }

    clearTexture();

    TTF_Init();

    loadFont("ASCII.ttf", 14, 255, 255, 255);
    loadFont("ASCII.ttf", 45, 255, 0, 0); //first font is put on "stack"

    mainc.setPos(0, 0);

	idbar.setPreferredSize(200, 100);
	statusBar.setPreferredSize(300, 100);
	list.setPreferredSize(800,800);

	signinMouseButtonListener(&list);
	signinMouseMotionListener(&list);

    setTotalBytesUploaded(0);
    setTotalBytesDownloaded(0);

    update();

    std::cout << "done" << std::endl;
}

void GUIScreen::setClient(client *cl)
{
	this->cl = cl;
}

bolo GUIScreen::alive(void)
{
	return !dead;
}
void GUIScreen::die(void)
{
	dead = true;
}
void GUIScreen::onExit(void)
{
	die();
}

void GUIScreen::onWindowResize(int x, int y)
{
	SCREEN_X = x;
	SCREEN_Y = y;
	//std::cout << "Window resized to: " << SCREEN_X << " " << SCREEN_Y << std::endl;
	update();
	updateSize();
}

void GUIScreen::onFileDropped(const char* fname)
{
	filename = std::string(fname, strlen(fname));
	std::cout << filename << std::endl;
	displayInputId();
}

void GUIScreen::clearTexture(void)
{
	SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255); //background black full alpha
	SDL_RenderClear(renderer);
}
void GUIScreen::updateSize()
{
	SDL_SetWindowSize(window, SCREEN_X, SCREEN_Y);
	SDL_DestroyTexture(texture);
	texture = SDL_CreateTexture(renderer, SDL_PIXELFORMAT_ARGB8888, SDL_TEXTUREACCESS_STATIC, SCREEN_X, SCREEN_Y);
}
void GUIScreen::update()
{
	mainc.update(SCREEN_X, SCREEN_Y);
	if(mainc.getX()!=SCREEN_X || mainc.getY()!=SCREEN_Y)
	{
		SCREEN_X = mainc.getX();
		SCREEN_Y = mainc.getY();
		updateSize();
	}

}
void GUIScreen::prepareDraw(void) //call to commit drawings to texture
{
	SDL_UpdateTexture(texture, NULL, mainc.getSurface()->pixels, mainc.getSurface()->pitch);
	SDL_Rect offset;
	std::deque<Box*> msgs = getMsgs();
	if(!msgs.empty())
	{
		offset.w = msgs.back()->getX();
		offset.h = msgs.back()->getY();
		if(offset.w>SCREEN_X || offset.h>SCREEN_Y)
		{
			SCREEN_X = max(offset.w, SCREEN_X);
			SCREEN_Y = max(offset.h, SCREEN_Y);
			updateSize();
			update();
			msgs.back()->setPos((SCREEN_X - offset.w) / 2, (SCREEN_Y - offset.h) / 2);
		}
		offset.x = msgs.back()->getPosX();
		offset.y = msgs.back()->getPosY();
		//std::cout << "drawing input box at " << offset.x << " " << offset.y << " " << offset.w << " " << offset.h;
		SDL_UpdateTexture(texture, &offset, msgs.back()->getSurface()->pixels, msgs.back()->getSurface()->pitch);
	}
	//std::cout << "done prepare draw" << std::endl;
}
void GUIScreen::draw(void) //call to output texture once to display
{
	SDL_RenderCopy(renderer, texture, NULL, NULL);
	SDL_RenderPresent(renderer); //SDL_Flip(screen);
}

//MSGBOX LAUNCHER

void GUIScreen::addNewMsgBox(Box* n)
{
	n->setPos((SCREEN_X-n->getX())/2, (SCREEN_Y-n->getY())/2);
	signinPopupBox(n);
}

bool GUIScreen::accept_incoming(char* file)
{
	MsgAccept* n = new MsgAccept(file);
	addNewMsgBox(n);
	bolo choice = n->getChoice();
	signoutPopupBox(n);
	delete n;
	//return true; //very good design
	return choice; //...very bad design //but who cares?
}
void GUIScreen::displayError(char* msg)
{
	addNewMsgBox(new MsgOk(msg));
}
void GUIScreen::connectFailed(char* msg)
{
	displayError(msg);
}
void GUIScreen::connectLost(char* msg)
{
	displayError(msg);
}
ProgressBar* GUIScreen::addProgressBar(std::string file, bolo b)
{
	ProgressBar* n = new ProgressBar(file, b);
	list.addComponent(n);
	//signinMouseButtonListener(n);
	return n;
}
void GUIScreen::setId(std::string nid)
{
	//id="Your ID in this session is: " + nid;
	id = nid;
	idbar.updateId(id);
}

void beautifyBytes(double b, char* output)
{
	int exp = 0;
	while(b / 1000 >= 1)
	{
		b /= 1000;
		exp++;
	}
	char c;
	if(exp == 0) c = ' ';
	else if(exp == 1) c = 'K';
	else if(exp == 2) c = 'M';
	else if(exp == 3) c = 'G';
	else if(exp == 4) c = 'T';
	else if(exp == 5) c = 'E';
	else if(exp == 6) c = 'P';
	sprintf(output, "%.3f%cB", b, c);
}

void GUIScreen::updateStatus(void)
{
	std::ostringstream oss;
	char tbd_s[64], tbu_s[64], sbd_s[64], sbu_s[64];
	beautifyBytes(tbd, tbd_s);
	beautifyBytes(tbu, tbu_s);
	beautifyBytes(sbd, sbd_s);
	beautifyBytes(sbu, sbu_s);
	oss << "Total Bytes Downloaded: " << tbd_s << "\n";
	oss << "Total Bytes Uploaded: " << tbu_s << "\n";
	oss << "Download Speed: " << sbd_s << "/s\n";
	oss << "Upload Speed: " << sbu_s << "/s";
	statusBar.updateText(oss.str().c_str());
}

void GUIScreen::setTotalBytesUploaded(double u)
{
	tbu=u;
	updateStatus();
}
void GUIScreen::setTotalBytesDownloaded(double d)
{
	tbd=d;
	updateStatus();
}
void GUIScreen::setUploadSpeed(double u)
{
	sbu=u;
	updateStatus();
}
void GUIScreen::setDownloadSpeed(double d)
{
	sbd=d;
	updateStatus();
}

void GUIScreen::idInputConfirmed(std::string str)
{
	cl->initiate_transfer(filename.c_str(), str.c_str());
}

void GUIScreen::displayInputId()
{
	MsgInput* n = new MsgInput(std::string("Input partner ID"), this);
	addNewMsgBox(n);
}

GUIScreen::~GUIScreen(void)
{
    std::cout << "Someone is closing mee!\n";

    SDL_DestroyTexture(texture);
    SDL_Quit();

    std::cout << "SDL IS DEAD!!!\n";
}
