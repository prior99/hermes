#ifndef __XSDLINCL__
#define __XSDLINCL__

#include"xEventHandler.hpp"

#include"msgBoxes/progressBar.hpp"
#include"msgBoxes/messageAccept.hpp"
#include"msgBoxes/messageOk.hpp"
#include"msgBoxes/listComponent.hpp"
#include"msgBoxes/rowComponent.hpp"
#include"msgBoxes/labelComponent.hpp"
#include"msgBoxes/mainComponent.hpp"
#include"msgBoxes/idComponent.hpp"
#include"msgBoxes/scrollComponent.hpp"
#include"guiListener.hpp"
#include<deque>
#include<string>

class client; //Forward declaration

class GUIScreen : public EventHandler, public GUIListener
{
private:
	SDL_Texture* texture; //surface->pixels -> texture
	SDL_Renderer* renderer;	//using renderer
	SDL_Window* window; //display on sdl2 window

	double tbu ,tbd, sbu, sbd;

	bolo dead;

	std::string id;

	//Component main
	MainComp mainc;

	IdComp idbar;
	LabelComp statusBar;
	ScrollComp list;

	int SCREEN_X;
	int SCREEN_Y;

	void updateStatus(void);
	void updateSize(void);

	//from EventHandler
	void onExit(void);
	void onFileDropped(const char*);
	void onWindowResize(int, int);
	void mouseDownAt(int, int);
	void mouseMotion(int, int, int, int);
	void mouseUpAt(int, int);

	client *cl;
	std::string filename;
public:
	GUIScreen();
	virtual ~GUIScreen();
	bolo alive(void);
	void die(void);

	void update();
	void clearTexture(void);
	void prepareDraw(void);
	void draw(void);

	void setTotalBytesUploaded(double);
	void setTotalBytesDownloaded(double);
	void setUploadSpeed(double);
	void setDownloadSpeed(double);
	void setId(std::string);
	void setProgressOfTransfer(int, int);
	ProgressBar* addProgressBar(std::string, bool); //returns id
	void setClient(client*);

    void idInputConfirmed(std::string);

	//msgbox-launcher
	void addNewMsgBox(Box*);
	void displayError(char*);
	void connectFailed(char*);
	void connectLost(char*);
	void displayInputId();
	//blocking
	bool accept_incoming(char*);
};

#endif
