/*
 * xSDL.hpp
 *
 *  Created on: Sep 19, 2013
 *      Author: sascha
 */

#ifndef XSDL_HPP_
#define XSDL_HPP_

#include<SDL2/SDL.h>
#include<SDL2/SDL_ttf.h>

#include<string>
#include<string.h>
#include<vector>

int max(int, int);
int min(int, int);

std::vector<std::string> splitByNewline(std::string); //splits the string by \n, removing \n
void loadFont(const char*, int, int, int, int);
//TTF_Font* getLoadedFont(void);
//void setTextColor(int, int, int);
SDL_Surface* createSDLSurface(int, int);
void loadPixelFormat(void);
SDL_Surface* createTextSurface(std::string, int);
SDL_Surface* createTextSurface(const char*, int);
void renderTextureOnto(SDL_Surface*, SDL_Surface*, int, int, int, int);
void renderTextureWithOffset(SDL_Surface*, SDL_Surface*, int, int, int, int, int, int);
void fillColor(SDL_Surface*, int, int, int);
void fillColorWithBorder(SDL_Surface*, int, int, int);
void fillColorWithOffset(SDL_Surface*, SDL_Rect*, int, int, int);
std::string keyCodeToString(SDL_Keycode);

#endif /* XSDL_HPP_ */
