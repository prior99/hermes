/*
 * xSDL.cpp
 *
 *  Created on: Sep 19, 2013
 *      Author: sascha
 */

#include"xSDL.hpp"
#include<iostream>
#include"defines.hpp"

//WRAPPER FUNCTIONS

using namespace std;

Uint32 rm, gm, bm, am;
int bpp;

typedef struct MyFont MyFont;
struct MyFont
{
	SDL_Color color;
	TTF_Font* font;

};

std::vector<MyFont> myFonts;

int max(int a, int b)
{
	return (a<b) ? b : a;
}
int min(int a, int b)
{
	return (a>b) ? b : a;
}

void loadFont(const char* pth, int h, int r, int g, int b)
{
	MyFont x;
    x.font = TTF_OpenFont(pth, h);
    if(!x.font)
    {
        std::cout << "[CRITICAL] Could not load font: " << pth << std::endl;
        return;
    }
    x.color.r = r;
    x.color.g = g;
    x.color.b = b;
    myFonts.push_back(x);
}
SDL_Surface* createSDLSurface(int x, int y)
{
	return SDL_CreateRGBSurface(0, x, y, bpp, rm, gm, bm, am);
}
void screen_updateSize(void) //missing
{
	return;
	/*
    if( SDL_SetVideoMode(SCREEN_X, SCREEN_Y, 32, SDL_OPENGL) == 0)
    {
        std::cout << "Video mode set failed: " << SDL_GetError() << "\n";
        exit(1);
    }
    glViewport(0, 0, SCREEN_X, SCREEN_Y);
    glMatrixMode(GL_PROJECTION);
    glOrtho(0.0f, SCREEN_X, SCREEN_Y, 0.0f, -1.0f, 1.0f);
    glMatrixMode(GL_MODELVIEW);*/
}

void renderTextureWithOffset(SDL_Surface* t, SDL_Surface* d, int x, int y, int w, int h, int ox, int oy)
{
	SDL_Rect offset;
	offset.x = x;
	offset.y = y;
	offset.w = w;
	offset.h = h;
	SDL_Rect offset2;
	offset2.x = ox;
	offset2.y = oy;
	offset2.w = w;
	offset2.h = h;
	SDL_BlitSurface(t, &offset2, d, &offset);
}
void renderTextureOnto(SDL_Surface* t, SDL_Surface* d, int x, int y, int w, int h)
{
	renderTextureWithOffset(t, d, x, y, w, h, 0, 0);
}
void loadPixelFormat(void)
{
	SDL_PixelFormatEnumToMasks(SDL_PIXELFORMAT_ARGB8888, &bpp, &rm, &gm, &bm, &am);
}
void displayTextOnto(SDL_Surface* txt, SDL_Surface* dspl, int x, int y)
{
    renderTextureOnto(txt, dspl, x, y, txt->w, txt->h);
}
SDL_Surface* createTextSurface(std::string txt, int y)
{

	std::vector<std::string> split = splitByNewline(txt);
	std::vector<SDL_Surface*> ssurfaces;
	int mx=0;
	int my=0;
	for(std::vector<std::string>::iterator i=split.begin(); i!=split.end(); i++)
	{
		ssurfaces.push_back(createTextSurface((*i).c_str(),y));
		mx=max(mx, ssurfaces.back()->w);
		my+=ssurfaces.back()->h;
	}
	SDL_Surface* surface = createSDLSurface(mx, my);
	my = 0;
	fillColor(surface, 30, 30, 30);
	for(std::vector<SDL_Surface*>::iterator i=ssurfaces.begin(); i!=ssurfaces.end(); i++)
	{
		renderTextureOnto((*i), surface, 0, my, (*i)->w, (*i)->h);
		//std::cout << (*i)->w << " " << (*i)->h << std::endl; //uncomment for additional info
		my+=(*i)->h;
		SDL_FreeSurface(*i);
	}
	return surface;
}
SDL_Surface* createTextSurface(const char* txt, int x)
{
	return(TTF_RenderUTF8_Blended(myFonts.at(x).font, txt, myFonts.at(x).color));
}
void fillColor(SDL_Surface* surf, int r, int g, int b)
{
	Uint32 color = SDL_MapRGBA(surf->format, r, g, b, 255);
	SDL_FillRect(surf, NULL, color);
}
void fillColorWithOffset(SDL_Surface* surf, SDL_Rect* rect, int r, int g, int b)
{
	SDL_FillRect(surf, rect, SDL_MapRGBA(surf->format, r, g, b, 255));
}

std::string keyCodeToString(SDL_Keycode k)
{
	const char* val = SDL_GetKeyName(k);
	return std::string(val, strlen(val));
}
void fillColorWithBorder(SDL_Surface* s, int r, int g, int b)
{
	fillColor(s,r+100,g+100,b+100);
	SDL_Rect o;
	o.w = s->w - 2*PADDING;
	o.h = s->h - 2*PADDING;
	o.x = o.y = PADDING;
	fillColorWithOffset(s, &o, r, g, b);
}
std::vector<std::string> splitByNewline(std::string str)
{
	std::vector<std::string> lines;
	std::string sline = "";
	for(std::string::iterator i=str.begin(); i!=str.end(); i++)
	{
		if((*i)=='\n')
		{
			lines.push_back(sline);
			sline = "";
		}
		else sline += (*i);
	}
	if(sline != "") lines.push_back(sline);
	return lines;
}


//--------------PROBABLY USELESS NOW---------------------

/*SDL_Surface* convert(const char* s)
{
    std::cout << "Converting: " << s << " ...";
    SDL_Surface *t1, *t2;
    t1 = SDL_LoadBMP(s);
    if(t1 != NULL) t2 = SDL_DisplayFormat(t1);
    if(t2 == NULL)
    {
        std::cout << "[CRITICAL] " << s << " file not found! Check path.";
        return NULL;
    }
    std::cout << "Done" << std::endl;
    SDL_FreeSurface(t1);
    return t2;
}*/
