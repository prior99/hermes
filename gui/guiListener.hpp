#ifdef __GUILISTENERINCL__
#else
#define __GUILISTENERINCL__

class GUIListener
{
public:
    virtual void idInputConfirmed(std::string) = 0;
};

#endif
