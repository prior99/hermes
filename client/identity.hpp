#ifndef class_identity
#define class_identity

	class identity
	{
		public:
			int id;
			identity();
			void readable_id(char *string);
			bool inited;
			char *key;
			void init(int id);
			int integer_id(const char*);
		private:
			void generate_key();
	};

#endif
