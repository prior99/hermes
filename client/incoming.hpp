#ifndef class_incoming
#define class_incoming

#include <fstream>
#include <iostream>
#include <boost/bind.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/asio.hpp>
#include "connection_client.hpp"
#include "../shared/packet_transmission_closed.hpp"
#include "../shared/packet_request_part.hpp"
#include "../gui/guiScreen.hpp"
#include "../gui/msgBoxes/progressBar.hpp"
#define packet_size 16000

using boost::asio::ip::udp;

class incoming
{
	public:
		incoming(unsigned int, unsigned int, int, char*, udp::endpoint*, connection_client*, ProgressBar*);
		void next_part(unsigned int, char*, int);
		void finished();
		int id;
	private:
		void request_part(unsigned int part);
		void check();
		bool *done;
		unsigned int len;
		unsigned int parts;
		char* filename;
		bool requesting;
		unsigned int parts_done;
		ProgressBar* bar;
		udp::endpoint *endpoint;
		connection_client *conn;
		std::ofstream f;
};

#endif
