#include "transmission.hpp"

using namespace std;
using namespace boost;
using namespace asio;
using boost::asio::ip::udp;

transmission::transmission(const char * filename, int id, connection_client *conn, const char *path, ProgressBar *bar) : f(path, std::ifstream::in | std::ifstream::binary)
{
	this->bar = bar;
	cout << "New transmission " << id << endl;
	this->id = id;
	this->filename = filename;
	this->path = path;
	this->conn = conn;
	if(f.good() && f.is_open())
	{
		f.seekg(0, std::ifstream::end);
		len = f.tellg();
		parts = len / packet_size;
		if(len % packet_size != 0) parts++;
		cout << "File " << path << " successfully analyzed: " << len << " bytes -> " << parts << " parts" << endl;
	}
	else
		cout << "Problem reading file " << path << endl;
	current = 0;

}

void transmission::begin()
{
	std::cout << "Beginning transmission" << std::endl;
}


void transmission::connect(const char host[], unsigned short int port, asio::io_service *ios)
{

	stringstream s;
	s << port;
	udp::resolver *resolver = new udp::resolver(*ios);
	udp::resolver::query query(udp::v4(), host, s.str().c_str());
	endpoint = resolver->resolve(query)->endpoint();
	//unsigned int len, int id, unsigned int parts, char* filename, size_t name_len
	packet_initiate_transfer pck(len, id, parts, filename, strlen(filename));
	conn->send(pck, &endpoint);
	cout << "Connected to " << host << ":" << port << endl;

}

void transmission::done()
{
	std::cout << "Transmission terminated" << std::endl;
	bar->remove();
	free(buffer);
}

void transmission::request_part(unsigned int id)
{
	//cout << "Got request for part " << id << endl;
	next_part(id);
}

void transmission::send_finished(const boost::system::error_code& error)
{
	//cout << "Send was done " << current << "/" << parts << endl;
	if(!error)
		current++;
	bar->updateProgress((int)((current/(float)parts)*100));

}

void transmission::next_part(unsigned int pid)
{
	//cout << "Sending part " << pid << endl;
	unsigned int start = packet_size*pid, end;
	if(start + packet_size > len) end = len;
	else end = start + packet_size;
	buffer = (char*)realloc(buffer, packet_size);
	if(f.good() && f.is_open())
	{
		f.seekg(start, ios_base::beg);
		f.read(buffer, end - start);
		//cout << "Successfully read " << (end - start) << "bytes to transfer at " << start << endl;
	}
	else
		cout << "Unabled to open inputfile" << endl;
	packet_file_part *pck = new packet_file_part(buffer, (int)(end - start), pid, id);
	conn->send(*pck, &endpoint, this);
	//cout << "Sended" << endl;
}
