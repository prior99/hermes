#ifndef class_transmission
#define class_transmission

#include <fstream>
#include <iostream>
#include <sstream>
#include "../gui/guiScreen.hpp"
#include "../gui/msgBoxes/progressBar.hpp"
#include <boost/array.hpp>
#include <boost/bind.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/asio.hpp>
#include "connection_client.hpp"
#include "../shared/packet_initiate_transfer.hpp"
#include "../shared/packet_file_part.hpp"
#include "../shared/send_handler.hpp"
#include "connection_client.hpp"
#define packet_size 16000

using boost::asio::ip::udp;

class transmission : public send_handler
{
	public:
		transmission(const char *, int id, connection_client*, const char *, ProgressBar *);
		void connect(const char[], unsigned short int, boost::asio::io_service*);
		void next_part(unsigned int);
		void request_part(unsigned int);
		void send_finished(const boost::system::error_code&);
		void done();
		void begin();
		ProgressBar* bar;
	private:
		int id;
		unsigned int len;
		unsigned int parts;
		unsigned int current;
		udp::endpoint endpoint;
		const char *filename;
		const char *path;
		connection_client *conn;
		std::fstream f;
		char *buffer;
};

#endif
