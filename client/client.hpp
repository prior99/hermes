#ifndef class_client
#define class_client

#include <iostream>
#include <boost/thread.hpp>
#include <string>
#include "../gui/guiScreen.hpp"
#include "../gui/msgBoxes/progressBar.hpp"
#include <boost/array.hpp>
#include <boost/asio.hpp>
#include "connection_client.hpp"
#include "incoming.hpp"
#include "transmission.hpp"
#include "identity.hpp"
#include "../shared/packet.hpp"
#include "../shared/packet_msg.hpp"
#include "../shared/packet_create_identity.hpp"
#include "../shared/packet_login.hpp"
#include <map>
#include <vector>

using boost::asio::ip::udp;

class client
{
	public:
		client(boost::asio::io_service&, char[], char[]);
		void init_identity(int id);
		void connect(char *id);
		identity ident;
		void new_incoming(unsigned int, int, unsigned int, char*, size_t, udp::endpoint *);
		void new_transfer(char[], unsigned short int, int echo);
		incoming *get_incoming(int);
		transmission *get_transmission(int);
		void close_transmission(int id);
		void initiate_transfer(const char *filename, const char *id);
		GUIScreen *x;
		char * rid;
		connection_client conn;
		boost::asio::io_service *ios;
	private:
		std::map<int, transmission*> transmissions;
		std::vector<incoming*> incomings;
		int transmission_amount;
};

#endif
