#ifndef class_connection_client
#define class_connection_client
#include <boost/array.hpp>
#include <boost/bind.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/asio.hpp>
#include "../shared/connection.hpp"
#include "../shared/packet.hpp"
#include "../shared/packet_msg.hpp"
#include "../shared/packet_identity.hpp"
#include "../shared/packet_connect.hpp"
#include "../shared/packet_connection.hpp"
#include "../shared/packet_login_status.hpp"
#include "../shared/packet_initiate_transfer.hpp"
#include "../shared/packet_file_part.hpp"
#include "../shared/packet_request_part.hpp"
#include "../shared/packet_transmission_finished.hpp"
#include "../shared/packet_transmission_closed.hpp"
#include "../shared/packet_answer_incoming.hpp"
#include "../shared/packet_punch.hpp"
#include "../shared/packet_answer_punch.hpp"
#include <queue>

using boost::asio::ip::udp;

class client;

class connection_client : public connection
{
	public:
		connection_client(boost::asio::io_service&, char[], char[], client*);
		void send_server(packet&);
		void handle(packet*, udp::endpoint*);
	private:
		udp::resolver resolver;
		udp::endpoint server_endpoint;
		client* parent;
};

#endif
