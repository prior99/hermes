#include "identity.hpp"
#include <stdlib.h>
#include <fstream>
#include <ctime>
#include <iostream>

using namespace std;

identity::identity()
{
	ifstream f("identity.dat");
	if(f.is_open() && f.good())
	{
		key = new char[64];
		f >> id;
		f.read(key, 64);
		inited = true;
		std::cout << "Identity successfully read from file." << std::endl;
	}
	else
	{
		generate_key();
		std::cout << "No identityfile found. Generating new key." << std::endl;
		inited = false;
		id = 0;
	}
	f.close();
}

void identity::init(int id)
{
	this->id = id;
	ofstream f("identity.dat");
	std::cout << "Saving identityfile..." << std::endl;
	if(f.is_open())
	{
		f << id;
		f.write(key, 64);
		std::cout << "Successfully saved identityfile." << std::endl;
	}
	else
	{
		std::cout << "Failed to save identity." << std::endl;
	}
	f.close();
}

void identity::readable_id(char *string)
{
	char idents[] = {'0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'};
	int i = id;
	//char erg[5];
	int rad = 36*36*36, it = 0;
	while(rad > 0)
	{
		string[it] = idents[i / rad];
		i %= rad;
		rad /= 36;
		it++;
	}
	string[4] = '\0';
}

int identity::integer_id(const char* string)
{
	int num, erg = 0, exp = 1;
	char c;
	for(int i = 3 ; i >= 0; i--)
	{
		c = string[i];
		if(c >= 'A' && c <= 'Z')
		{
			num = 10 + c - 'A';
		}
		else if(c >= '0' && c <= '9')
		{
			num = c - '0';
		}
		else
		{
			std::cout << "ERROR: Invalid character!" << endl;
			num = -1;
			return -1;
		}
		erg += num * exp;
		exp *= 36;
	}
	return erg;
}

void identity::generate_key()
{
	key = new char[64];
	char c;
	srand(time(0));
	for(int i = 0; i < 64; i++)
	{
		c = rand()%255;
		key[i] = c;
	}
}
