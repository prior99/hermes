#include "incoming.hpp"

using namespace std;
using boost::asio::ip::udp;

incoming::incoming(unsigned int len, unsigned int parts, int id, char* filename, udp::endpoint *endpoint, connection_client *conn, ProgressBar* bar) : f(filename, ios::binary | ios::out)
{
	this->bar = bar;
	this->parts = parts;
	this->filename = filename;
	cout << "New incoming " << id << ", expecting " << parts << " parts from file " << filename << "with size of " << len << "bytes" <<endl;
	done = (bool*) malloc(parts);
	for(int i = 0; i < parts; i++)
		done[i] = false;
	ofstream fo(filename);
	fo.seekp(len - 1);
	fo << '\0';
	fo.close();
	requesting = false;
	this->endpoint = endpoint;
	this->conn = conn;
	this->id = id;
	parts_done = 0;
	request_part(parts_done);
}

void incoming::next_part(unsigned int part, char* content, int size)
{
	//cout << "Received part " << part << " writing " << size << "bytes to "<< part * packet_size << endl;
	f.seekp(part * packet_size, ios_base::beg);
	f.write(content, size);
	done[part] = true;
	parts_done++;
	bar->updateProgress((int)((parts_done/(float)parts)*100));
	free(content);
	if(parts_done >= parts) finished();
	else if(requesting) check();
	else request_part(parts_done);
}

void incoming::finished()
{
	f.flush();
	f.close();
	bar->remove();
	cout << "Tranmission seems to be done" << endl;
	requesting = true;
	check();
}

void incoming::check()
{
	cout << "Checking for missing parts" << endl;
	for(int i = 0; i < parts; i++)
	{
		if(!done[i])
		{
			cout << "Part " << i << " missing, requesting..."  << endl;
			request_part(i);
			return;
		}
	}
	//Done
	f.flush();
	f.close();
	free(done);
	cout << "Filetransfer completed!" << endl;
	packet_transmission_closed *pck = new packet_transmission_closed(id);
	conn->send(*pck, endpoint);

}

void incoming::request_part(unsigned int part)
{
	//cout << "Requesting part " << part << " for id  "<< id << " from " << endpoint << endl;
	packet_request_part *pck = new packet_request_part(id, part);
	conn->send(*pck, endpoint);
}
