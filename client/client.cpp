#include "client.hpp"
#include "../shared/packet_connect.hpp"
#include "../gui/msgBoxes/messageAccept.hpp"
using namespace boost;
using namespace asio;
using namespace std;
#include <string>

//#include<iostream>
void gui_worker(client *cl)
{

	GUIScreen *x = new GUIScreen();
	cl->x = x;
	x->setClient(cl);
	x->setId(cl->rid);
	//ProgressBar* y = x->addProgressBar("asdasd", ProgressBar::PUPLOAD);
	//y->setPreference(ScrollComp::POS_BOUNDRIGHT);
	//y->updateProgress(100);
	//y->remove();
	double bdl = 0, bul = 0, bds = 0, bus = 0;

	while(x->alive())
	{
		bds = (cl->conn.received_bytes - bdl) * 10;
		bus = (cl->conn.sended_bytes - bul) * 10;

		bdl = cl->conn.received_bytes;
		bul = cl->conn.sended_bytes;
		x->setDownloadSpeed(bds);
		x->setUploadSpeed(bus);
		x->setTotalBytesDownloaded(bdl);
		x->setTotalBytesUploaded(bul);
		x->handleEvents();
		x->update();
		x->prepareDraw();
		x->draw();
		SDL_Delay(100);
		//std::cout << "cycle" << std::endl;
	}
    //notify hermes of requested close
}

client::client(io_service &io_service, char host[], char port[]) : conn(io_service, host, port, this)
{
	//gui.join();
	transmission_amount = 0;
	this->ios = &io_service;
	if(!ident.inited)
	{
		packet_create_identity pck = packet_create_identity(ident.key);
		conn.send_server(pck);
		cout << "Waiting for identity from server" << endl;
	}
	else
		init_identity(ident.id);
}


void client::new_incoming(unsigned int len, int id, unsigned int parts, char* filename, size_t name_len, udp::endpoint *ep)
{
	if(x->accept_incoming(filename))
	{
		ProgressBar *y = x->addProgressBar(filename, ProgressBar::PDOWNLOAD);
		y->setPreference(ListComp::POS_BOUNDLEFT);
		packet_answer_incoming pck = packet_answer_incoming(id, true);
		conn.send(pck, ep);
		incoming *in = new incoming(len, parts, id, filename, ep, &conn, y);
		incomings.push_back(in);
	}
	else
	{
		packet_answer_incoming pck = packet_answer_incoming(id, true);
		conn.send(pck, ep);
	}
}

incoming *client::get_incoming(int id)
{
	incoming *in = 0;
	for(int i = 0; i < incomings.size(); i++)
	{
		if(incomings[i]->id == id)
		{
			in = incomings[i];
			break;
		}
	}
	return in;
}

void client::close_transmission(int id)
{
	get_transmission(id)->done();
	transmissions.erase(id);
}

transmission *client::get_transmission(int id)
{
	if(transmissions.count(id) > 0)
	{
		return transmissions.at(id);
	}
	else return 0;
}

void client::initiate_transfer(const char *filename, const char *id)
{
	char sep;
	#ifdef _WIN32
		sep = '\\';
	#else
		sep = '/';
	#endif
	int iid = ident.integer_id(id);
	if(iid == -1)
	{
		cout << "Invalid ID!" << endl;
		return;
	}
	string s(filename);
	int i;
	for(i = s.length(); i > 0; --i)
	{
		if(s[i] == sep) break;
	}
	i++;
	string name = s.substr(i, s.length() - i);
	transmission_amount++;
	char *c_name = new char[name.length()+1];
	for(int i = 0; i < name.length(); i++)
		c_name[i] = name.c_str()[i];
	c_name[name.length()] = '\0';
	ProgressBar* y = x->addProgressBar(name.c_str(), ProgressBar::PUPLOAD);
	y->setPreference(ScrollComp::POS_BOUNDLEFT);

	transmission *t = new transmission(c_name, transmission_amount, &conn, filename, y);
	transmissions[transmission_amount] = t;
	cout << "Created new transmission with ID " << transmission_amount << endl;
	packet_connect pck(iid, transmission_amount);
	conn.send_server(pck);
}

void client::new_transfer(char host[], unsigned short int port, int echo)
{
	transmission* t = get_transmission(echo);
	if(t == 0)
		cout << "Invalid transmission ID" << endl;
	else
		t->connect(host, port, ios);
}

void client::init_identity(int id)
{
	ident.init(id);
	//char rid[4];
	rid = new char[4];
	ident.readable_id(rid);
	cout << "Initialized, id is \"" << rid << "\"" << endl;
	cout << "Authenticating..." << endl;
	packet_login pck = packet_login(ident.id, ident.key);
	conn.send_server(pck);
	//packet_connect pck2 = packet_connect(ident.integer_id(rid));
	//conn.send(pck2);
	//x->setId(std::string(rid, 4));
	boost::thread gui(gui_worker, this);
}

void client::connect(char *id)
{
	/*int iid = ident.integer_id(id);
	packet_connect pck = packet_connect(iid);
	conn.send_server(pck);*/
}



int main(int args, char** argv)
{

	char host[] = "localhost";
	char port[] = "13";
	try
	{
		boost::asio::io_service io_service;
		client client(io_service, host, port);
		io_service.run();
	}
	catch(std::exception& e)
	{
		cerr << e.what() << endl;
	}
	return 0;
}
