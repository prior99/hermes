#include "connection_client.hpp"
#include "client.hpp"

using namespace boost;
using namespace asio;
using namespace std;

connection_client::connection_client(boost::asio::io_service &ios, char host[], char port[], client* parent) : resolver(ios), connection()
{
	socket = new udp::socket(ios);
	this->parent = parent;
	socket->open(udp::v4());
	udp::resolver::query query(udp::v4(), host, port);
	server_endpoint = resolver.resolve(query)->endpoint();
	listen();
}

void connection_client::send_server(packet &packet)
{
	udp::endpoint *ep = &server_endpoint;
	send(packet, ep);
}


void connection_client::handle(packet *pack, udp::endpoint *endpoint)
{
	packet_id id = pack->get_id();

	switch(id)
	{
		case id_answer_incoming:
		{
			cout << "Received packet answer incoming" << endl;
			packet_answer_incoming *pck = (packet_answer_incoming*)pack;
			if(pck->ok)
				parent->get_transmission(pck->id)->begin();
			else
				parent->close_transmission(pck->id);
			break;
		}
		case id_msg:
		{
			packet_msg *pck = (packet_msg*)pack;
			cout << "Message: \"" << pck->msg << "\"" << endl;
			break;
		}
		case id_identity:
		{
			packet_identity *pck = (packet_identity*)pack;
			parent->init_identity(pck->id);
			break;
		}
		case id_punch:
		{
			packet_answer_punch *pck2 = new packet_answer_punch(parent->ident.id);
			send(*pck2, endpoint);
			cout << "Got punch, sending answer" << endl;
			break;
		}
		case id_answer_punch:
		{
			packet_answer_punch *pck = (packet_answer_punch*)pack;
			char id[5];
			parent->ident.readable_id(id, pck->id);
			id[4] = '\0';
			cout << "Successfully established connection to \"" << id << "\"" << endl;
			break;
		}
		case id_connection:
		{
			packet_connection *pck = (packet_connection*)pack;
			unsigned short int port = pck->port;
			char* host = pck->host;
			cout << "Connect to: " << host << ", " << port << " (Transfer-ID: " << pck->echo << ")" << endl;

			packet_punch *pck2 = new packet_punch();
			stringstream s;
			s << pck->port;
			udp::resolver resolver(*parent->ios);
			udp::resolver::query query(udp::v4(), pck->host, s.str().c_str());
			udp::endpoint endpoint = resolver.resolve(query)->endpoint();
			send(*pck2, &endpoint);

			if(pck->echo != 0)
				parent->new_transfer(host, port, pck->echo);
			break;
		}
		case id_login_status:
		{
			packet_login_status *pck = (packet_login_status*)pack;
			bool success = pck->ok;
			if(success) cout << "Loginattempt was successfull" << endl;
			else cout << "Loginattempt failed!" << endl;
			break;
		}
		case id_file_part:
		{
			packet_file_part *pck = (packet_file_part*)pack;
			incoming *in = parent->get_incoming(pck->id);
			if(in != 0)
				in->next_part(pck->part, pck->data, pck->size);
			break;
		}
		case id_initiate_transfer:
		{
			packet_initiate_transfer *pck = (packet_initiate_transfer*)pack;
			parent->new_incoming(pck->len, pck->id, pck->parts, pck->filename, pck->name_len, endpoint);
			break;
		}
		case id_transmission_closed:
		{
			packet_transmission_closed *pck = (packet_transmission_closed*)pack;
			parent->close_transmission(pck->id);
			break;
		}
		case id_transmission_finished:
		{
			packet_transmission_finished *pck = (packet_transmission_finished*)pack;
			parent->get_incoming(pck->id)->finished();
			break;
		}
		case id_request_part:
		{
			packet_request_part *pck = (packet_request_part*)pack;
			parent->get_transmission(pck->id)->request_part(pck->part);
			break;
		}
		default:
		{
			cout << "Received unexpected packet with id " << id << endl;
			break;
		}
	}
}
