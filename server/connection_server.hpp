#ifndef class_connection_server
#define class_connection_server

#include <boost/array.hpp>
#include <boost/bind.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/asio.hpp>
#include "../shared/connection.hpp"
#include "../shared/packet.hpp"
#include "../shared/packet_msg.hpp"
#include "../shared/packet_create_identity.hpp"
#include "../shared/packet_identity.hpp"
#include "../shared/packet_login.hpp"
#include "../shared/packet_login_status.hpp"
#include "../shared/packet_connect.hpp"
#include "../shared/packet_connection.hpp"
#include <queue>

class server;

using boost::asio::ip::udp;

	class connection_server : public connection
	{
		public:
			connection_server(boost::asio::io_service&, int, server*);
		protected:
			void handle(packet *, udp::endpoint*);
		private:
			server* parent;
	};

#endif
