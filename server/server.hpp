#include <iostream>
#include <boost/array.hpp>
#include <boost/bind.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/asio.hpp>
#include "identity_base.hpp"
#include "connection_server.hpp"

using boost::asio::ip::udp;

class server
{
	public:
		server(boost::asio::io_service&, int);
		identity_base identities;
	private:
		connection_server conn;

};
