#include "server.hpp"
using namespace boost;
using namespace asio;
using namespace std;

server::server(io_service& io_service, int port) : conn(io_service,  port, this)
{
}


int main()
{
	try
	{
		io_service io_service;
		server server(io_service, 13);
		io_service.run();
	}
	catch (std::exception& e)
	{
		std::cerr << e.what() << std::endl;
	}
	return 0;
}
