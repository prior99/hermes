#include "identity_base.hpp"
#include <iostream>
#include <fstream>

using boost::asio::ip::udp;

identity_base::identity_base()
{
	keys = std::vector<char*>();
	endpoints = std::vector<udp::endpoint*>();
	saving = false;
	load();
}

void identity_base::save()
{
	if(saving) return;
	saving = true;
	std::cout << "Saving identities..." << std::endl;
	std::ofstream f("identities.dat");
	if(f.is_open())
	{
		f << highest_id;
		for(int i = 0; i < highest_id; i++)
		{
			f.write(keys[i], 64);
		}
		std::cout << "Identities successfully saved" << std::endl;
	}
	else
	{
		std::cout << "Failed to save identities" << std::endl;
	}
	f.close();
	saving = false;
}

void identity_base::load()
{
	std::ifstream f("identities.dat");
	std::cout << "Loading identities..." << std::endl;
	if(f.is_open())
	{
		f >> highest_id;
		std::cout << "Next id is " << highest_id << std::endl;
		for(int i = 0; i < highest_id; i++)
		{
			char *c = new char[64];
			f.read(c, 64);
			keys.push_back(c);
		}
		std::cout << "Identities successfully loaded." << std::endl;
	}
	else
	{
		std::cout << "No identities file found. Starting at 0." << std::endl;
		highest_id = 0;
	}
	endpoints.resize(highest_id+1, 0);
	f.close();
}

bool identity_base::identity_exists(int id)
{
	return id < highest_id;
}

bool identity_base::key_matches(int id, char* key)
{
	if(!identity_exists(id)) return false;
	for(int i = 0; i < 64; i++)
	{
		if(key[i] != keys[id][i]) return false;
	}
	return true;
}

void identity_base::login(int id, udp::endpoint* endpoint)
{
	std::cout << id << "->" << endpoint->port() << std::endl;
	endpoints[id] = endpoint;
}

int identity_base::create_new_identity(char* key)
{
	endpoints.resize(highest_id+1, 0);
	int id = highest_id++;
	keys.push_back(key);
	save();
	return id;
}
