#ifndef class_identity_base
#define class_identity_base

#include <vector>
#include <boost/asio.hpp>


	class identity_base
	{
		public:
			identity_base();
			bool identity_exists(int);
			bool key_matches(int, char*);
			int create_new_identity(char*);
			void login(int, boost::asio::ip::udp::endpoint*);
			std::vector<boost::asio::ip::udp::endpoint*> endpoints;
		private:
			void load();
			void save();
			unsigned int highest_id;
			bool saving;
			std::vector<char*> keys;
	};

#endif
