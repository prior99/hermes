#include "connection_server.hpp"
#include "server.hpp"

using namespace boost;
using namespace asio;
using namespace std;

connection_server::connection_server(boost::asio::io_service& io_service, int port, server* parent) : connection()
{
	socket = new udp::socket(io_service, udp::endpoint(udp::v4(), port));
	this->parent = parent;
	listen();
}

void connection_server::handle(packet *pack, udp::endpoint *endpoint)
{
	packet_id id = pack->get_id();
	switch(id)
	{
		case id_msg:
		{
			packet_msg *pck = (packet_msg*)pack;
			cout << "Message: \"" << pck->msg << "\"" << endl;
			break;
		}
		case id_login:
		{
			packet_login *pck = (packet_login*)pack;
			if(parent->identities.key_matches(pck->id, pck->key))
			{
				packet_login_status pck2 = packet_login_status(true);
				send(pck2, endpoint);
				parent->identities.login(pck->id, endpoint);
				cout << pck->id << " successfully logged in" << endl;
			}
			else
			{
				packet_login_status pck2 = packet_login_status(false);
				send(pck2, endpoint);
				cout << pck->id << " attempted login but failed" << endl;
			}
			cout << "Adress is: " << endpoint->address() << ":" << endpoint->port() << endl;
			break;
		}
		case id_create_identity:
		{
			packet_create_identity *pck = (packet_create_identity*)pack;
			char* key = pck->key;
			int id = parent->identities.create_new_identity(key);
			cout << "Creating new identity with id " << id << endl;
			packet_identity pckid(id);
			send(pckid, endpoint);
			break;
		}
		case id_connect:
		{
			packet_connect *pck = (packet_connect*)pack;
			int id = pck->id;
			cout << "Request for " << id << endl;
			if(parent->identities.identity_exists(id))
			{
				udp::endpoint *ep = parent->identities.endpoints[id];
				packet_connection *pck2 = new packet_connection(ep->address().to_string().c_str(), ep->port(), pck->echo);
				cout << "Sending connection: " << ep->address() << ":" << ep->port() << endl;
								send(*pck2, endpoint);

				packet_connection *pck3 = new packet_connection(endpoint->address().to_string().c_str(), endpoint->port(), 0);
				send(*pck3, ep);
				cout << "Sending connection: " << ep->address() << ":" << ep->port() << endl;

			}
			else
			{
				cout << "Invalid request. No such id " << id << endl;
			}
			break;
		}
		default:
		{
			cout << "Received unexpected packet with id " << id << endl;
			break;
		}
	}
}
