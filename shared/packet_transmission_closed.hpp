#ifndef class_packet_transmission_closed
#define class_packet_transmission_closed

#include "packet.hpp"

class packet_transmission_closed : public packet
{
	public:
		packet_transmission_closed()
		{
		}
		packet_transmission_closed(int id)
		{
			this->id = id;
		}
		void _write()
		{
			write_int(id);

		}
		void _read()
		{
			id = read_int();
		}

		packet_id get_id()
		{
			return id_transmission_closed;
		}
		size_t packet_size()
		{
			return 4;
		}
		int id;
};

#endif
