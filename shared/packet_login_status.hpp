#ifndef class_packet_login_status
#define class_packet_login_status

#include "packet.hpp"

class packet_login_status : public packet
{
	public:
		packet_login_status()
		{

		}

		packet_login_status(bool ok)
		{
			this-> ok = ok;
		}

		void _write()
		{
			write_bool(true);
		}

		void _read()
		{
			ok = read_bool();
		}

		packet_id get_id()
		{
			return id_login_status;
		}

		size_t packet_size()
		{
			return 1;
		}
		bool ok;
};

#endif
