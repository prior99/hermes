#ifndef class_packet_connect
#define class_packet_connect

class packet_connect : public packet
{
	public:
		packet_connect()
		{

		}
		packet_connect(int id, int echo)
		{
			this->echo =echo;
			this->id = id;
		}
		void _write()
		{
			write_int(id);
			write_int(echo);

		}
		void _read()
		{
			id = read_int();
			echo = read_int();
		}
		packet_id get_id()
		{
			return id_connect;
		}
		size_t packet_size()
		{
			return 4 + 4;
		}
		int id;
		int echo;
};

#endif
