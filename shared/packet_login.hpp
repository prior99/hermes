#ifndef class_packet_login
#define class_packet_login

#include "packet.hpp"

class packet_login : public packet
{
	public:
		packet_login()
		{

		}

		packet_login(int id, char* key)
		{
			this->id = id;
			this->key = key;
		}

		void _write()
		{
			write_int(id);
			write_string(key, 64);
		}

		void _read()
		{
			id = read_int();
			key = (char*)malloc(64);
			read_string(key, 64);
		}

		packet_id get_id()
		{
			return id_login;
		}

		size_t packet_size()
		{
			return 4 + 64;
		}
		int id;
		char *key;
};
#endif
