#ifndef class_packet_initiate_transfer
#define class_packet_initiate_transfer

#include "packet.hpp"

class packet_initiate_transfer : public packet
{
	public:
		packet_initiate_transfer() {};
		packet_initiate_transfer(unsigned int len, int id, unsigned int parts, const char* filename, unsigned int name_len)
		{
			this->filename = new char[name_len];
			for(int i = 0; i< name_len; i++)
				this->filename[i] = filename[i];
			this->parts = (int)parts;
			this->id = id;
			this->len = len;
			this->name_len = name_len;
		};
		void _write()
		{
			write_u_int(len);
			write_int(id);
			write_u_int(parts);
			write_u_int(name_len);
			write_string(filename, name_len);
		};
		void _read()
		{
			len = read_u_int();
			id = read_int();
			parts = read_u_int();
			name_len = read_u_int();
			filename = new char[name_len + 1];
			read_string(filename, name_len);
			filename[name_len] = '\0';
		};

		packet_id get_id()
		{
			return id_initiate_transfer;
		};

		size_t packet_size()
		{
			return name_len + 4 + 4 + 4 + 4;
		}
		char *filename;
		unsigned int parts;
		int id;
		unsigned int len;
		unsigned int name_len;
};

#endif
