#ifndef class_packet_answer_incoming
#define class_packet_answer_incoming

#include "packet.hpp"

class packet_answer_incoming : public packet
{
	public:
		packet_answer_incoming() {}
		packet_answer_incoming(int id, bool ok)
		{
			this->id = id;
			this->ok = ok;
		}
		void _write()
		{
			write_int(id);
			write_bool(ok);
		};
		void _read()
		{
			id = read_int();
			ok = read_bool();
		};
		packet_id get_id()
		{
			return id_answer_incoming;
		};
		size_t packet_size()
		{
			return 4 + 1;
		}
		int id;
		bool ok;
};

#endif
