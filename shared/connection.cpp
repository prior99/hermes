#include "connection.hpp"
#include "packet.hpp"

using namespace boost;
using namespace asio;
using namespace std;

connection::connection()
{
	buff_in = (char*) malloc(16284);
	buff_out = (char*) malloc(1);
}

void connection::listen()
{
	//buff_in = (char*)malloc(buff_in, 16384);
	udp::endpoint *target = new udp::endpoint();
	socket->async_receive_from(buffer(buff_in, 16384), *target, bind(&connection::recv, this, placeholders::error, placeholders::bytes_transferred, buff_in, target));
}

void connection::send(packet& pck, udp::endpoint *endpoint)
{
	send(pck, endpoint, 0);
}
void connection::send(packet& pck, udp::endpoint *endpoint, send_handler * handler)
{
	packet_target_pair pair;
	pair.endpoint = endpoint;
	pair.pck = &pck;
	pair.handler = handler;
	queue.push(pair);
	pop_queue();
}

void connection::pop_queue()
{
	if(!busy && !queue.empty())
	{
		busy = true;
		packet_target_pair pair = queue.front();
		queue.pop();
		send_(*pair.pck, *pair.endpoint, pair.handler);
	}
}

void connection::recv(const boost::system::error_code& error, std::size_t msg_size, char* buff, udp::endpoint* endpoint)
{
	packet * pck;
	listen();
	if(!error)
	{
		packet_id id = (packet_id)buff[0];
		//cout << "Received packet with id " << (int)id << endl;
		/*cout << endl;
		cout << "RECV ";
		for(int i = 0; i < msg_size; i++)
			cout << "[" << (int)buff[i] << "]";
		cout << endl;*/
		msg_size--;
		buff++;
		received_bytes += msg_size;
		switch(id)
		{
			case id_msg:					{ pck = new packet_msg(); 					break; }
			case id_create_identity:		{ pck = new packet_create_identity(); 		break; }
			case id_identity:				{ pck = new packet_identity(); 				break; }
			case id_login:					{ pck = new packet_login(); 				break; }
			case id_login_status:			{ pck = new packet_login_status(); 			break; }
			case id_connect:				{ pck = new packet_connect(); 				break; }
			case id_connection:				{ pck = new packet_connection(); 			break; }
			case id_initiate_transfer:		{ pck = new packet_initiate_transfer(); 	break; }
			case id_file_part:				{ pck = new packet_file_part(); 			break; }
			case id_request_part:			{ pck = new packet_request_part(); 			break; }
			case id_transmission_finished:	{ pck = new packet_transmission_finished();	break; }
			case id_transmission_closed:	{ pck = new packet_transmission_closed();	break; }
			case id_answer_incoming:		{ pck = new packet_answer_incoming();		break; }
			case id_punch:					{ pck = new packet_punch();					break; }
			case id_answer_punch:			{ pck = new packet_answer_punch();			break; }
			default:
			{
				cout << "Received known packet but not registered in connection" << endl;
				return;
			}
		}
		pck->read(buff, msg_size);
		//free(buff);
		handle(pck, endpoint);
		//delete pck;
	}
	else
	{
		cout << "Dropping a packet: "<< error.message() << endl;
	}
}

void connection::send_(packet& pck, udp::endpoint endpoint, send_handler *handler)
{
	buff_out = (char*) realloc(buff_out, pck.packet_size() + 1);
	sended_bytes += pck.packet_size() + 1;
	pck.write(buff_out+1);
	buff_out[0] = pck.get_id();
	/*cout << endl;
	cout << "SEND ";
	for(int i = 0; i < pck.packet_size() + 1; i++)
		cout << "[" << (int)buff[i] << "]";
	cout << endl;*/
	//cout << "Sending packet with id " << (packet_id)buff[0] << endl;
	socket->async_send_to(asio::buffer(buff_out, pck.packet_size() + 1), endpoint, bind(&connection::send_done, this, placeholders::error, handler, buff_out));
}

void connection::send_done(const system::error_code& error, send_handler *handler, char* buff)
{
	//free(buff);
	if(error != 0) cout << error.message() << endl;
	if(handler != 0) handler->send_finished(error);
	busy = false;
	pop_queue();
}
