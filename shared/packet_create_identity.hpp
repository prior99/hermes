#ifndef class_packet_create_identity
#define class_packet_create_identity

#include "packet.hpp"

	class packet_create_identity : public packet
	{
		public:
			packet_create_identity()
			{

			}
			packet_create_identity(char* key)
			{
				this->key = key;
			}
			void _write()
			{
				write_string(key, 64);
			}
			void _read()
			{
				key = (char*)malloc(64);
				read_string(key, 64);
			}
			packet_id get_id()
			{
				return id_create_identity;
			}
			size_t packet_size()
			{
				return 64;
			}
			char* key;
	};

#endif
