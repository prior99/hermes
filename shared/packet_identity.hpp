#ifndef class_packet_identity
#define class_packet_identity

#include "packet.hpp"

class packet_identity : public packet
{
	public:
		packet_identity()
		{

		}
		packet_identity(int id)
		{
			this->id = id;
		}
		void _write()
		{
			write_int(id);

		}
		void _read()
		{
			id = read_int();
		}
		packet_id get_id()
		{
			return id_identity;
		}
		size_t packet_size()
		{
			return 4;
		}
		int id;
};

#endif
