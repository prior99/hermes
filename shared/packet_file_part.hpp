#ifndef class_packet_file_part
#define class_packet_file_part

#include <iostream>
#include "packet.hpp"

class packet_file_part : public packet
{
	public:
		packet_file_part()
		{

		}
		packet_file_part(char* data, int size, unsigned int part, int id)
		{
			this->id = id;
			this->size = size;
			this->part = part;
			this->data = data;
		}
		void _write()
		{
			write_int(size);
			write_int(id);
			write_u_int(part);
			write_string(data, size);
		}
		void _read()
		{
			size = read_int();
			id = read_int();
			part = read_u_int();
			data = (char*)malloc(size);
			read_string(data, size);
		}
		packet_id get_id()
		{
			return id_file_part;
		}
		size_t packet_size()
		{
			return 4 + 4 + 4 + size;
		}
		int id;
		unsigned int part;
		int size;
		char* data;
};

#endif
