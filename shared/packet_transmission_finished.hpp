#ifndef class_packet_transmission_finished
#define class_packet_transmission_finished

#include "packet.hpp"

class packet_transmission_finished : public packet
{
	public:
		packet_transmission_finished()
		{
		}
		packet_transmission_finished(int id)
		{
			this->id = id;
		}
		void _write()
		{
			write_int(id);

		}
		void _read()
		{
			id = read_int();
		}

		packet_id get_id()
		{
			return id_transmission_finished;
		}
		size_t packet_size()
		{
			return 4;
		}
		int id;
};

#endif
