#ifndef class_packet_test
#define class_packet_test

#include "packet.hpp"

class packet_msg : public packet
{
	public:
		packet_msg() {};
		packet_msg(char *msg, size_t msg_size)
		{
			this->msg = msg;
			this->msg_size = msg_size;
		};
		void _write()
		{
			write_int(msg_size);
			write_string(msg, msg_size);
		};
		void _read()
		{
			msg_size = read_int();
			msg = (char*)malloc(msg_size);
			read_string(msg, msg_size);
		};
		packet_id get_id()
		{
			return id_msg;
		};
		size_t packet_size()
		{
			return msg_size + 4;
		}
		char *msg;
		size_t msg_size;
};

#endif
