#ifndef class_packet_answer_punch
#define class_packet_answer_punch

#include "packet.hpp"

class packet_answer_punch : public packet
{
	public:
		packet_answer_punch(int id)
		{
			this->id = id;
		}
		packet_answer_punch()
		{
		};
		void _write()
		{
			write_int(id);
		};
		void _read()
		{
			id = read_int();
		};
		packet_id get_id()
		{
			return id_answer_punch;
		};
		size_t packet_size()
		{
			return 0;
		}
		int id;
};

#endif
