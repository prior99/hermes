#ifndef class_packet_connection
#define class_packet_connection

#include <string.h>

class packet_connection : public packet
{
	public:
		packet_connection()
		{

		}
		packet_connection(const char *host, int port, int echo)
		{
			si_host = strlen(host);
			this-> host = new char[si_host];
			for(int i = 0; i < si_host; i++) this->host[i] = host[i];
			this->port = port;
			this->echo = echo;
		}

		void _write()
		{
			write_u_short(port);
			write_char(si_host);
			write_int(echo);
			write_string(host, si_host);

		}
		void _read()
		{
			port = read_u_short();
			si_host = read_char();
			echo = read_int();
			host = new char[si_host + 1];
			read_string(host, si_host);
			host[si_host] = '\0';
		}
		packet_id get_id()
		{
			return id_connection;
		}
		size_t packet_size()
		{
			return si_host + 2 + 1 + 4;
		}
		char *host;
		int port;
		char si_host;
		int echo;
};

#endif
