#ifndef class_packet
#define class_packet

enum packet_id
{
	id_msg					 =  1,
	id_create_identity		 =  2,
	id_identity				 =  3,
	id_login				 =  4,
	id_login_status			 =  5,
	id_connect				 =  6,
	id_connection			 =  7,
	id_initiate_transfer	 =  8,
	id_file_part			 =  9,
	id_request_part			 = 10,
	id_transmission_finished = 11,
	id_transmission_closed	 = 12,
	id_answer_incoming		 = 13,
	id_punch				 = 14,
	id_answer_punch			 = 15
};

class packet
{
	public:
		void write(char *buffer)
		{
			this->buffer = (unsigned char*)buffer;
			offset = 0;
			this->_write();
		}
		void read(char *buffer, size_t)
		{
			this->buffer = (unsigned char*)buffer;
			offset = 0;
			this->_read();
		}
		virtual packet_id get_id() = 0;
		virtual size_t packet_size() = 0;
	protected:

		virtual void _write() = 0;
		virtual void _read() = 0;

		void write_int(int i)
		{
			buffer[offset + 3] = (i >> (8 * 3)) & 0xFF;
			buffer[offset + 2] = (i >> (8 * 2)) & 0xFF;
			buffer[offset + 1] = (i >> (8 * 1)) & 0xFF;
			buffer[offset + 0] = (i >> (8 * 0)) & 0xFF;
			//std::cout << "     wrote INT " << i << " at offset " << offset  << std::endl;
			offset += 4;
		}

		void write_u_int(unsigned int i)
		{
			unsigned char *buffer_u = (unsigned char*) buffer;
			buffer_u[offset + 3] = (i >> (8 * 3)) & 0xFF;
			buffer_u[offset + 2] = (i >> (8 * 2)) & 0xFF;
			buffer_u[offset + 1] = (i >> (8 * 1)) & 0xFF;
			buffer_u[offset + 0] = (i >> (8 * 0)) & 0xFF;
			//std::cout << "     wrote UINT " << i << " at offset " << offset  << std::endl;
			offset += 4;
		}

		unsigned int read_u_int()
		{
			unsigned char *buffer_u = (unsigned char*) buffer;
			unsigned int i;
			i =  buffer_u[offset + 3] << (8 * 3);
			i = (buffer_u[offset + 2] << (8 * 2)) | i;
			i = (buffer_u[offset + 1] << (8 * 1)) | i;
			i = (buffer_u[offset + 0] << (8 * 0)) | i;
			//std::cout << "     read UINT " << i << " at offset " << offset  << std::endl;
			offset += 4;
			return i;
		}

		void write_u_short(unsigned short int i)
		{
			unsigned char *buffer_u = (unsigned char*) buffer;
			buffer_u[offset + 1] = (i >> (8 * 1)) & 0xFF;
			buffer_u[offset + 0] = (i >> (8 * 0)) & 0xFF;
			//std::cout << "     wrote USHORT " << i << " at offset " << offset << "(" << (int)buffer_u[offset + 1] << ", " << (int)buffer_u[offset + 0] << ")"<< std::endl;
			offset += 2;
		}

		unsigned short int read_u_short()
		{
			unsigned char *buffer_u = (unsigned char*) buffer;
			unsigned short int i;
			i = (buffer_u[offset + 1] << (8 * 1));
			i = (buffer_u[offset + 0] << (8 * 0)) | i;
			//std::cout << "     read USHORT " << i << " at offset " << offset << "(" << (int)buffer_u[offset + 1] << ", " << (int)buffer_u[offset + 0] << ")"<< std::endl;
			offset += 2;
			return i;
		}

		int read_int()
		{
			int i;
			i =  buffer[offset + 3] << (8 * 3);
			i = (buffer[offset + 2] << (8 * 2)) | i;
			i = (buffer[offset + 1] << (8 * 1)) | i;
			i = (buffer[offset + 0] << (8 * 0)) | i;
			//std::cout << "     read INT " << i << " at offset " << offset  << std::endl;
			offset += 4;
			return i;
		}

		void write_char(char c)
		{
			buffer[offset] = c;
			//std::cout << "     wrote CHAR " << (int)c << " at offset " << offset << std::endl;
			offset++;
		}

		void write_string(char * str, int size)
		{
			for(int i = 0; i < size; i++)
				buffer[i + offset] = str[i];
			//std::cout << "     wrote STRING of length " << size << " at offset " << offset  << std::endl;
			offset += size;
		}

		char read_char()
		{
			char c = buffer[offset];
			//std::cout << "     read CHAR " << (int)c << " at offset " << offset  << std::endl;
			offset ++;
			return c;
		}

		void read_string(char *str, int size)
		{
			for(int i = 0; i < size; i++)
				str[i] = buffer[i + offset];
			//std::cout << "     read STRING of length " << size << " at offset " << offset  << std::endl;
			offset += size;
		}

		void write_bool(bool b)
		{
			if(b) buffer[offset] = 1;
			else buffer[offset] = 0;
		//	std::cout << "     wrote BOOL " << b << " at offset " << offset  << std::endl;
			offset++;
		}

		bool read_bool()
		{
			bool b = buffer[offset] == 1;
			//std::cout << "     read BOOL " << b << " at offset " << offset  << std::endl;
			offset++;
			return b;
		}

		unsigned char *buffer;
		size_t offset;

	private:

};

#endif
