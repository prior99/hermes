#ifndef class_packet_request_part
#define class_packet_request_part

#include "packet.hpp"

class packet_request_part : public packet
{
	public:
		packet_request_part()
		{

		}
		packet_request_part(int id, unsigned int part)
		{
			this->id = id;
			this->part = part;
		}
		void _write()
		{
			write_int(id);
			write_u_int(part);

		}
		void _read()
		{
			id = read_int();
			part = read_u_int();
		}
		packet_id get_id()
		{
			return id_request_part;
		}
		size_t packet_size()
		{
			return 4 + 4;
		}
		int id;
		unsigned int part;
};

#endif
