#ifndef class_connection
#define class_connection

#include <boost/array.hpp>
#include <boost/bind.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/asio.hpp>
#include "send_handler.hpp"
#include "packet.hpp"
#include "packet_msg.hpp"
#include "packet_create_identity.hpp"
#include "packet_identity.hpp"
#include "packet_login.hpp"
#include "packet_login_status.hpp"
#include "packet_connect.hpp"
#include "packet_connection.hpp"
#include "packet_initiate_transfer.hpp"
#include "packet_file_part.hpp"
#include "packet_request_part.hpp"
#include "packet_transmission_finished.hpp"
#include "packet_transmission_closed.hpp"
#include "packet_answer_incoming.hpp"
#include "packet_punch.hpp"
#include "packet_answer_punch.hpp"
#include <queue>

class server;

using boost::asio::ip::udp;

	struct packet_target_pair
	{
		packet* pck;
		udp::endpoint* endpoint;
		send_handler *handler;
	};

	class connection
	{
		public:
			connection();
			void send(packet&, udp::endpoint *endpoint);
			void send(packet&, udp::endpoint *endpoint, send_handler *handler);
			double received_bytes, sended_bytes;
		protected:
			virtual void handle(packet*, udp::endpoint*) = 0;
			udp::socket *socket;
			void listen();
		private:
			void send_done(const boost::system::error_code&, send_handler *handler, char *buff);
			void recv(const boost::system::error_code&, std::size_t, char*, udp::endpoint*);
			void send_(packet&, udp::endpoint endpoint, send_handler *handler);
			void pop_queue();
			bool busy;
			std::queue<packet_target_pair> queue;
			boost::asio::io_service io_service;
			char *buff_in;
			char *buff_out;
	};

#endif
