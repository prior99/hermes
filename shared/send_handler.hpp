#ifndef class_send_handler
#define class_send_handler

class send_handler
{
	public:
		virtual void send_finished(const boost::system::error_code&) = 0;
};

#endif
