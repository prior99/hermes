include Makefile.rules
default: all
.PHONY: server client
all:clean server client

server:
	make -C server all
	
client:
	make -C client all
	
	
clean:
	@echo -e "\033[1;33mCleaning...\033[39m"
	$(RM) $(SERVER_BIN)
	$(RM) $(CLIENT_BIN)
	@echo -e "\033[0;32mDone.\033[39m"
